--
-- PostgreSQL database dump
--

-- Dumped from database version 15.2
-- Dumped by pg_dump version 15.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: adherent; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.adherent (
    id_adherent integer NOT NULL,
    nom character varying(50),
    prenom character varying(50),
    email character varying(50),
    user_password character varying(50),
    user_role character varying(50) NOT NULL,
    solde numeric(10,2)
);


ALTER TABLE public.adherent OWNER TO postgres;

--
-- Name: adherent_id_adherent_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.adherent_id_adherent_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.adherent_id_adherent_seq OWNER TO postgres;

--
-- Name: adherent_id_adherent_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.adherent_id_adherent_seq OWNED BY public.adherent.id_adherent;


--
-- Name: app_user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.app_user (
    id_app_user integer NOT NULL,
    nom character varying(50),
    prenom character varying(50),
    email character varying(50),
    user_password character varying(50),
    user_role character varying(50) NOT NULL
);


ALTER TABLE public.app_user OWNER TO postgres;

--
-- Name: app_user_id_app_user_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.app_user_id_app_user_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.app_user_id_app_user_seq OWNER TO postgres;

--
-- Name: app_user_id_app_user_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.app_user_id_app_user_seq OWNED BY public.app_user.id_app_user;


--
-- Name: emprunt; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.emprunt (
    id_emprunt integer NOT NULL,
    date_emprunt date NOT NULL,
    id_ouvrage integer NOT NULL,
    id_adherent integer NOT NULL,
    date_reservation date,
    id_reservation integer
);


ALTER TABLE public.emprunt OWNER TO postgres;

--
-- Name: emprunt_id_emprunt_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.emprunt_id_emprunt_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.emprunt_id_emprunt_seq OWNER TO postgres;

--
-- Name: emprunt_id_emprunt_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.emprunt_id_emprunt_seq OWNED BY public.emprunt.id_emprunt;


--
-- Name: figurer; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.figurer (
    id_oeuvre integer NOT NULL,
    id_personne integer NOT NULL
);


ALTER TABLE public.figurer OWNER TO postgres;

--
-- Name: oeuvre; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.oeuvre (
    id_oeuvre integer NOT NULL,
    titre character varying(50),
    genre character varying(50),
    annee_parution date,
    exemplaires integer,
    exemplaires_dispo integer,
    prix numeric(10,2)
);


ALTER TABLE public.oeuvre OWNER TO postgres;

--
-- Name: oeuvre_id_oeuvre_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.oeuvre_id_oeuvre_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.oeuvre_id_oeuvre_seq OWNER TO postgres;

--
-- Name: oeuvre_id_oeuvre_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.oeuvre_id_oeuvre_seq OWNED BY public.oeuvre.id_oeuvre;


--
-- Name: ouvrage; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ouvrage (
    id_ouvrage integer NOT NULL,
    disponible boolean,
    reserve boolean,
    a_supprimer boolean,
    id_oeuvre integer NOT NULL
);


ALTER TABLE public.ouvrage OWNER TO postgres;

--
-- Name: ouvrage_id_ouvrage_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.ouvrage_id_ouvrage_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ouvrage_id_ouvrage_seq OWNER TO postgres;

--
-- Name: ouvrage_id_ouvrage_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.ouvrage_id_ouvrage_seq OWNED BY public.ouvrage.id_ouvrage;


--
-- Name: personne; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.personne (
    id_personne integer NOT NULL,
    nom character varying(50),
    prenom character varying(50),
    profession character varying(50) NOT NULL
);


ALTER TABLE public.personne OWNER TO postgres;

--
-- Name: personne_id_personne_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.personne_id_personne_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.personne_id_personne_seq OWNER TO postgres;

--
-- Name: personne_id_personne_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.personne_id_personne_seq OWNED BY public.personne.id_personne;


--
-- Name: reservation; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.reservation (
    id_reservation integer NOT NULL,
    date_reservation date NOT NULL,
    id_ouvrage integer NOT NULL,
    id_adherent integer NOT NULL
);


ALTER TABLE public.reservation OWNER TO postgres;

--
-- Name: reservation_id_reservation_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.reservation_id_reservation_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.reservation_id_reservation_seq OWNER TO postgres;

--
-- Name: reservation_id_reservation_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.reservation_id_reservation_seq OWNED BY public.reservation.id_reservation;


--
-- Name: adherent id_adherent; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.adherent ALTER COLUMN id_adherent SET DEFAULT nextval('public.adherent_id_adherent_seq'::regclass);


--
-- Name: app_user id_app_user; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_user ALTER COLUMN id_app_user SET DEFAULT nextval('public.app_user_id_app_user_seq'::regclass);


--
-- Name: emprunt id_emprunt; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.emprunt ALTER COLUMN id_emprunt SET DEFAULT nextval('public.emprunt_id_emprunt_seq'::regclass);


--
-- Name: oeuvre id_oeuvre; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.oeuvre ALTER COLUMN id_oeuvre SET DEFAULT nextval('public.oeuvre_id_oeuvre_seq'::regclass);


--
-- Name: ouvrage id_ouvrage; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ouvrage ALTER COLUMN id_ouvrage SET DEFAULT nextval('public.ouvrage_id_ouvrage_seq'::regclass);


--
-- Name: personne id_personne; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.personne ALTER COLUMN id_personne SET DEFAULT nextval('public.personne_id_personne_seq'::regclass);


--
-- Name: reservation id_reservation; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reservation ALTER COLUMN id_reservation SET DEFAULT nextval('public.reservation_id_reservation_seq'::regclass);


--
-- Data for Name: adherent; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.adherent (id_adherent, nom, prenom, email, user_password, user_role, solde) FROM stdin;
1	DUPONT	NADEGE	nadeged@gmail.com	root	ADHERENT	100.00
2	DUPIRE	JULES	jules@gmail.com	root	ADHERENT	50.00
3	DUMONt	MARIE	mariesd@gmail.com	root	ADHERENT	70.00
\.


--
-- Data for Name: app_user; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.app_user (id_app_user, nom, prenom, email, user_password, user_role) FROM stdin;
1	DUMONT	JEROME	jerome@gmail.com	root	ADMIN)
2	DUPIRE	JULES	julesd@gmail.com	root	MEDIATHECAIRE
3	MAIETTE	VALERIE	valeriem@gmail.com	root	MEDIATHECAIRE
4	DUPIRE	STEPHANE	staphanedd@gmail.com	root	MEDIATHECAIRE
\.


--
-- Data for Name: emprunt; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.emprunt (id_emprunt, date_emprunt, id_ouvrage, id_adherent, date_reservation, id_reservation) FROM stdin;
\.


--
-- Data for Name: figurer; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.figurer (id_oeuvre, id_personne) FROM stdin;
1	1
2	1
3	2
3	3
4	3
\.


--
-- Data for Name: oeuvre; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.oeuvre (id_oeuvre, titre, genre, annee_parution, exemplaires, exemplaires_dispo, prix) FROM stdin;
1	I am not your Soulmate	LIVRE	2023-02-08	10	10	1.10
2	Un visage pour deux	LIVRE	2021-06-23	5	5	1.10
3	My son	FILM	2020-09-20	3	3	3.00
4	Millenium	FILM	2009-05-13	4	4	3.00
\.


--
-- Data for Name: ouvrage; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.ouvrage (id_ouvrage, disponible, reserve, a_supprimer, id_oeuvre) FROM stdin;
1	f	f	f	3
\.


--
-- Data for Name: personne; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.personne (id_personne, nom, prenom, profession) FROM stdin;
1	MARS	LYLA	AUTEUR
2	MCAVOY	JAMES	ACTEUR
3	FOY	CLAIRE	ACTEUR
\.


--
-- Data for Name: reservation; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.reservation (id_reservation, date_reservation, id_ouvrage, id_adherent) FROM stdin;
\.


--
-- Name: adherent_id_adherent_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.adherent_id_adherent_seq', 3, true);


--
-- Name: app_user_id_app_user_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.app_user_id_app_user_seq', 4, true);


--
-- Name: emprunt_id_emprunt_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.emprunt_id_emprunt_seq', 1, false);


--
-- Name: oeuvre_id_oeuvre_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.oeuvre_id_oeuvre_seq', 4, true);


--
-- Name: ouvrage_id_ouvrage_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.ouvrage_id_ouvrage_seq', 2, true);


--
-- Name: personne_id_personne_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.personne_id_personne_seq', 3, true);


--
-- Name: reservation_id_reservation_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.reservation_id_reservation_seq', 1, false);


--
-- Name: adherent adherent_email_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.adherent
    ADD CONSTRAINT adherent_email_key UNIQUE (email);


--
-- Name: adherent adherent_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.adherent
    ADD CONSTRAINT adherent_pkey PRIMARY KEY (id_adherent);


--
-- Name: app_user app_user_email_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_user
    ADD CONSTRAINT app_user_email_key UNIQUE (email);


--
-- Name: app_user app_user_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_user
    ADD CONSTRAINT app_user_pkey PRIMARY KEY (id_app_user);


--
-- Name: emprunt emprunt_id_ouvrage_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.emprunt
    ADD CONSTRAINT emprunt_id_ouvrage_key UNIQUE (id_ouvrage);


--
-- Name: emprunt emprunt_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.emprunt
    ADD CONSTRAINT emprunt_pkey PRIMARY KEY (date_emprunt);


--
-- Name: figurer figurer_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.figurer
    ADD CONSTRAINT figurer_pkey PRIMARY KEY (id_oeuvre, id_personne);


--
-- Name: oeuvre oeuvre_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.oeuvre
    ADD CONSTRAINT oeuvre_pkey PRIMARY KEY (id_oeuvre);


--
-- Name: ouvrage ouvrage_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ouvrage
    ADD CONSTRAINT ouvrage_pkey PRIMARY KEY (id_ouvrage);


--
-- Name: personne personne_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.personne
    ADD CONSTRAINT personne_pkey PRIMARY KEY (id_personne);


--
-- Name: reservation reservation_id_ouvrage_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reservation
    ADD CONSTRAINT reservation_id_ouvrage_key UNIQUE (id_ouvrage);


--
-- Name: reservation reservation_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reservation
    ADD CONSTRAINT reservation_pkey PRIMARY KEY (id_reservation);


--
-- Name: emprunt emprunt_id_adherent_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.emprunt
    ADD CONSTRAINT emprunt_id_adherent_fkey FOREIGN KEY (id_adherent) REFERENCES public.adherent(id_adherent);


--
-- Name: emprunt emprunt_id_ouvrage_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.emprunt
    ADD CONSTRAINT emprunt_id_ouvrage_fkey FOREIGN KEY (id_ouvrage) REFERENCES public.ouvrage(id_ouvrage);


--
-- Name: emprunt emprunt_id_reservation_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.emprunt
    ADD CONSTRAINT emprunt_id_reservation_fkey FOREIGN KEY (id_reservation) REFERENCES public.reservation(id_reservation);


--
-- Name: figurer figurer_id_oeuvre_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.figurer
    ADD CONSTRAINT figurer_id_oeuvre_fkey FOREIGN KEY (id_oeuvre) REFERENCES public.oeuvre(id_oeuvre);


--
-- Name: figurer figurer_id_personne_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.figurer
    ADD CONSTRAINT figurer_id_personne_fkey FOREIGN KEY (id_personne) REFERENCES public.personne(id_personne);


--
-- Name: ouvrage ouvrage_id_oeuvre_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ouvrage
    ADD CONSTRAINT ouvrage_id_oeuvre_fkey FOREIGN KEY (id_oeuvre) REFERENCES public.oeuvre(id_oeuvre);


--
-- Name: reservation reservation_id_adherent_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reservation
    ADD CONSTRAINT reservation_id_adherent_fkey FOREIGN KEY (id_adherent) REFERENCES public.adherent(id_adherent);


--
-- Name: reservation reservation_id_ouvrage_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reservation
    ADD CONSTRAINT reservation_id_ouvrage_fkey FOREIGN KEY (id_ouvrage) REFERENCES public.ouvrage(id_ouvrage);


--
-- PostgreSQL database dump complete
--

