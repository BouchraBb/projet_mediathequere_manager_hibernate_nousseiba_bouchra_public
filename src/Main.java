
import fr.afpa.mediatheque.controllers.*;

import fr.afpa.mediatheque.models.AppUser;

import fr.afpa.mediatheque.controllers.MediathecaireController;

import fr.afpa.mediatheque.utils.Constantes;

public class Main {
    public static void main(String[] args) {

        MediathecaireController mediathecaireController = new MediathecaireController();
        AdminController adminController = new AdminController();
        AppController appController = new AppController();
        AuthenticationController authenticationController = new AuthenticationController();
        String choice = "";
        AppUser appUser = new AppUser();

        do {
            if(args.length > 0) {
                try {
                    appUser = authenticationController.authenticate(args[0], args[1]);
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
                if(appUser.getUserRole().equals(Constantes.ROLE_ADMIN)) {
                    choice = adminController.displayMenu();
                }
                if(appUser.getUserRole().equals(Constantes.ROLE_MEDIATHECAIRE)) {
                    choice =  mediathecaireController.displayMenu();
                }
            }

            choice = appController.displayMenu();


        } while(!"0".equalsIgnoreCase(choice));
    }
}