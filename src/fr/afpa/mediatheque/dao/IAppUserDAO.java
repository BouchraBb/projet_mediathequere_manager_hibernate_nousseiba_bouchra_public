package fr.afpa.mediatheque.dao;

import fr.afpa.mediatheque.models.Adherent;
import fr.afpa.mediatheque.models.AppUser;

import java.util.List;

public interface IAppUserDAO {

    AppUser findByLogin(String login);


}
