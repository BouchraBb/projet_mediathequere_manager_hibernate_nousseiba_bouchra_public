package fr.afpa.mediatheque.dao;

import java.sql.Connection;
import java.util.List;

public interface ReservationEmpruntDAO<T> {
  //  public Connection connect = ConnectionPostgreSQL.getInstance();


    public  T findByIdOuvrage (long idOuvrage)  ;
    public List <T >finAlldByIdAdherent(long idAdherent)  ;

    List<T> findAllOutOfTime();
}
