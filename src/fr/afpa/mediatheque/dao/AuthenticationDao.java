package fr.afpa.mediatheque.dao;


import fr.afpa.mediatheque.models.Adherent;
import fr.afpa.mediatheque.models.AppUser;

import java.sql.Connection;

public interface AuthenticationDao {

    public Connection connect = null;
    public AppUser authenticate(String username, String password) throws Exception;

    public Adherent authenticateAdherent(String email, String userPassword);
}
