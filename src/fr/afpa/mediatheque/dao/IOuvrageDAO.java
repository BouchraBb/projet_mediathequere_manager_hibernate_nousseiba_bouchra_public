package fr.afpa.mediatheque.dao;

import fr.afpa.mediatheque.models.Ouvrage;

import java.util.List;

public interface IOuvrageDAO {
    public  void deleteOuvragesRendus();
    public List<Long> findOuvrages();

    List<Ouvrage> findOuvragesOeuvreDisponible(Long id);
}
