package fr.afpa.mediatheque.dao;

import fr.afpa.mediatheque.models.Oeuvre;

import java.util.List;

public interface IOeuvreDAO {

    Oeuvre findByTitle(String title);

    List<Oeuvre> findAndFilter(String[] keywords, List<String> str);
}
