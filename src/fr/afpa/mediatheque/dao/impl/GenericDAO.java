package fr.afpa.mediatheque.dao.impl;
import fr.afpa.mediatheque.utils.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class GenericDAO<T> {

    public static Session session =  HibernateUtil.getSessionFactory().openSession();
    private Class<T> entity;
    public GenericDAO(Class<T> entity) {
        this.entity = entity;
    }

    public Long save(T obj) {
        Transaction transaction = null;
        Long cle  ;
        try  {
            transaction = session.beginTransaction();
             cle = (Long)session.save(obj);
            System.out.println(cle);
            transaction.commit();
            return cle;

        } catch (Exception e) {
            e.printStackTrace();
            if (transaction != null) {
                System.out.println("error create");
                transaction.rollback();
            }
            return null ;
        }

    }

    public void update(T obj)  {
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.saveOrUpdate(obj);
            tx.commit();
        } catch (Exception e) {
            e.printStackTrace();
            if (tx != null){
                System.out.println("error update");
                tx.rollback();
            }
        }
    }
    public void delete(T obj)  {
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.delete(obj);
            tx.commit();
        } catch (Exception e) {
            if (tx != null){
                System.out.println("error delete");
                tx.rollback();
            }

        }
    }
    public T findById(long id) {

        return (T) session.get(entity, id);
        //session.getTransaction().commit();

    }

    public List<T> findAll() {
        return (List<T>) session.createQuery("from " + entity.getName()).list();
    }


}
