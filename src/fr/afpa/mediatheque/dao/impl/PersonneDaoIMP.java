package fr.afpa.mediatheque.dao.impl;

import fr.afpa.mediatheque.dao.DAO;
import fr.afpa.mediatheque.models.Oeuvre;
import fr.afpa.mediatheque.models.Personne;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class PersonneDaoIMP extends GenericDAO<Personne>  {


    public PersonneDaoIMP() {
        super(Personne.class);
    }
}
