package fr.afpa.mediatheque.dao.impl;

import fr.afpa.mediatheque.dao.IAppUserDAO;
import fr.afpa.mediatheque.models.Adherent;
import fr.afpa.mediatheque.models.AppUser;
import fr.afpa.mediatheque.dao.DAO;
import org.hibernate.Query;
import org.hibernate.Transaction;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AppUserDaoIMP extends GenericDAO<AppUser> implements IAppUserDAO {
    public AppUserDaoIMP() {
        super(AppUser.class);
    }

    @Override
    public Adherent findByLogin(String mail){
        Transaction transaction = session.beginTransaction();
        Query query = session.getNamedQuery("findByLogin");
        query.setParameter("mail", mail);
        Adherent adherent  = (Adherent) query.uniqueResult();
        System.out.println( adherent.getIdUser());
        transaction.commit();

        return adherent;
    }


 //
 //   public AppUser findByLogin(String login) {

 //       AppUser appUser = new AppUser();

//        String query = "SELECT * FROM app_user where email = '" + login + "';";
//
//        try {
//
//            ResultSet result = this.connect.createStatement().executeQuery(query);
//
//            while (result.next()){
//
//                appUser.setId(result.getLong("id_app_user"));
//                appUser.setNom(result.getString("nom"));
//                appUser.setPrenom(result.getString("prenom"));
//                appUser.setEmail(result.getString("email"));
//                appUser.setUserPassword(result.getString("user_password"));
//                appUser.setUserRole(result.getString("user_role"));
//            }
//        } catch (SQLException e) {
//            throw new RuntimeException(e);
//        }
//        return appUser;
//    }


}
