package fr.afpa.mediatheque.dao.impl;

import fr.afpa.mediatheque.dao.DAO;
import fr.afpa.mediatheque.dao.IAdherentDAO;
import fr.afpa.mediatheque.models.Adherent;
import org.hibernate.Query;
import org.hibernate.Transaction;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AdherentDaoIMP  extends GenericDAO<Adherent> implements  IAdherentDAO {


    public AdherentDaoIMP() {
        super(Adherent.class);
    }

    @Override
    public Adherent findByLogin(String mail){
        Transaction transaction = session.beginTransaction();
        Query query = session.getNamedQuery("findAdherentByLogin");
        query.setParameter("email", mail);
        Adherent adherent  = (Adherent) query.uniqueResult();
        System.out.println( adherent.getIdUser());
        transaction.commit();

        return adherent;
    }

    //  @Override
    //   public Adherent findByLogin(String login) {
    //       Adherent adherent = new Adherent();

//        String query = "SELECT * FROM Adherent WHERE email = '" + login + "'";

//        try {
//            ResultSet result = this.connect.createStatement().executeQuery(query);
//
//            while (result.next()){
//                adherent.setId(result.getLong("id_adherent"));
//                adherent.setNom(result.getString("nom"));
//                adherent.setPrenom(result.getString("prenom"));
//                adherent.setEmail(result.getString("email"));
//                adherent.setUserPassword(result.getString("user_password"));
//                adherent.setUserRole(result.getString("user_role"));
//                adherent.setSolde(result.getDouble("solde"));
//            }
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        return adherent;
    //   }
//}


}