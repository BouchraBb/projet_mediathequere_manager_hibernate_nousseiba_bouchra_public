package fr.afpa.mediatheque.dao.impl;

import fr.afpa.mediatheque.dao.DAO;
import fr.afpa.mediatheque.dao.IOuvrageDAO;
import fr.afpa.mediatheque.models.Oeuvre;
import fr.afpa.mediatheque.models.Ouvrage;
import org.hibernate.Query;
import org.hibernate.Transaction;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class OuvrageDaoIMP extends GenericDAO<Ouvrage> implements IOuvrageDAO {


    public OuvrageDaoIMP() {
        super(Ouvrage.class);
    }

    @Override
    public void deleteOuvragesRendus() {
        Transaction transaction = session.beginTransaction();
        Query query = session.getNamedQuery("deleteOuvragesRendus");
        transaction.commit();
    }

    //TODO A verifier
    @Override
    public List<Long> findOuvrages() {
        Transaction transaction = session.beginTransaction();
        Query query = session.getNamedQuery("findOuvrages");

        List<Ouvrage> ouvrages=(ArrayList<Ouvrage>) query.list();


        List<Long> IdOeuvres = new ArrayList<>();
        for (Ouvrage ouvrage: ouvrages ) {
            IdOeuvres.add(ouvrage.getOeuvre().getIdOeuvre());
        }

        transaction.commit();
        return IdOeuvres;
    }

    @Override
    public List<Ouvrage> findOuvragesOeuvreDisponible(Long id) {
        Transaction transaction = session.beginTransaction();
        Query query = session.getNamedQuery("deleteOuvragesRendus");
        ArrayList<Ouvrage> ouvrages=(ArrayList<Ouvrage>) query.list();
        query.setParameter("idOeuvre",id);

        transaction.commit();
        return ouvrages;
    }

//    @Override
//    public void deleteOuvragesRendus() {
//        try {
//            this.connect
//                    .createStatement(
//
//                    ).executeUpdate(
//                            "DELETE FROM Ouvrage WHERE  disponible=  true and a_supprimer=true ;"
//                    );
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }

//    }
//    public List<Long> findOuvrages()  {
//        OeuvreDaoIMP oeuvreDAO= new OeuvreDaoIMP();
//        List<Long> IdOeuvres = new ArrayList<>();
//        try {
//            ResultSet result = this.connect
//                    .createStatement(
//                    ).executeQuery(
//                            "SELECT id_oeuvre FROM Ouvrage WHERE  disponible=  true and a_supprimer=true ;"
//                    );
//
//            while (result.next()){
//               Long idOeuvre = result.getLong("id_oeuvre");
//                IdOeuvres.add(idOeuvre);
//            }
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        return IdOeuvres;
//    }

//    @Override
//    public List<Ouvrage> findOuvragesOeuvreDisponible(Long id) {
//        OeuvreDaoIMP oeuvreDAO= new OeuvreDaoIMP();
//        List<Ouvrage> ouvragesDispo = new ArrayList<>();
//        try {
//            ResultSet result = this.connect
//                    .createStatement(
//                    ).executeQuery(
//                            "SELECT * FROM Ouvrage WHERE id_oeuvre= "+id +" and disponible=true and reserve=false;"
//                    );
//
//            while (result.next()){
//                Ouvrage ouvrage = new Ouvrage();
//                ouvrage.setId(result.getLong("id_ouvrage"));
//                ouvrage.setAvailable(result.getBoolean("disponible"));
//                ouvrage.setRserved(result.getBoolean("reserve"));
//                ouvrage.setToBeDeleted(result.getBoolean("a_supprimer"));
//                Oeuvre oeuvre = oeuvreDAO.find(result.getLong("id_oeuvre"));
//                ouvrage.setOeuvre(oeuvre);
//                ouvragesDispo.add(ouvrage);
//            }
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        return ouvragesDispo;
//    }
}
