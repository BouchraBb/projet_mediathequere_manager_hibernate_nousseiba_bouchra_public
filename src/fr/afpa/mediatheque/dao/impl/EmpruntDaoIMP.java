package fr.afpa.mediatheque.dao.impl;

import fr.afpa.mediatheque.dao.DAO;
import fr.afpa.mediatheque.dao.ReservationEmpruntDAO;
import fr.afpa.mediatheque.models.Adherent;
import fr.afpa.mediatheque.models.Emprunt;
import fr.afpa.mediatheque.models.Ouvrage;
import fr.afpa.mediatheque.models.Reservation;
import org.hibernate.Query;
import org.hibernate.Transaction;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class EmpruntDaoIMP extends GenericDAO<Emprunt>  implements ReservationEmpruntDAO<Emprunt> {
    public EmpruntDaoIMP() {
        super(Emprunt.class);
    }


    //DAO<Ouvrage> ouvrageDAO= new OuvrageDaoIMP();
    //DAO<Adherent> adherentDAO= new AdherentDaoIMP();
    @Override
    public Emprunt findByIdOuvrage(long idOuvrage) {
        Transaction transaction = session.beginTransaction();
        Query query = session.getNamedQuery("findByIdOuvrage");
        query.setParameter("idOuvrage", idOuvrage);
        Emprunt emprunt = (Emprunt) query.uniqueResult();
        //System.out.println(emprunt.getIdEmprunt());

        return emprunt;
    }

    @Override
    public List<Emprunt> finAlldByIdAdherent(long idAdherent) {
        Transaction transaction = super.session.beginTransaction();
        Query query = session.getNamedQuery("finAlldByIdAdherent");
        query.setParameter("iduser", idAdherent);
        ArrayList<Emprunt> emprunts = (ArrayList<Emprunt>) query.list();
        return emprunts;
    }

    @Override
    public List<Emprunt> findAllOutOfTime() {
        Query query=session.createSQLQuery("SELECT * FROM Emprunt WHERE (current_date - dateemprunt) > 14");
        List<Emprunt> emprunts= (List<Emprunt>)   query.list();
        return emprunts;
    }


//    @Override
//    public List<Emprunt> findAllOutOfTime() {
//        Transaction transaction=super.session.beginTransaction();
//        Query query=session.getNamedQuery("findAllOutOfTime");
//        ArrayList<Emprunt> emprunts=(ArrayList<Emprunt>) query.list();
//        return emprunts;
//    }

//    @Override
//    public Emprunt findByIdOuvrage(long idOuvrage) {
//        Emprunt emprunt = new Emprunt();
//        try {
//            ResultSet result = this.connect
//                    .createStatement(
//                    ).executeQuery(
//                            "SELECT * FROM Emprunt WHERE id_ouvrage = " + idOuvrage
//                    );
//
//            while (result.next()){
//                emprunt.setId(result.getLong("id_emprunt"));
//                emprunt.setDateEmprunt(result.getDate("date_emprunt"));
//                Ouvrage ouvrage = ouvrageDAO.find(result.getLong("id_ouvrage"));
//                emprunt.setOuvrage(ouvrage);
//                Adherent adherent = adherentDAO.find(result.getLong("id_adherent"));
//                emprunt.setAdherent(adherent);
//            }
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        return emprunt;
//    }

//    @Override
//    public List<Emprunt> finAlldByIdAdherent(long idAdherent) {
//        List<Emprunt> empruntsAdherent = new ArrayList<>();
//        try {
//            ResultSet result = this.connect
//                    .createStatement(
//                    ).executeQuery(
//                            "SELECT * FROM Emprunt WHERE id_adherent = " + idAdherent
//                    );
//
//            while (result.next()){
//                Emprunt emprunt = new Emprunt();
//                emprunt.setId(result.getLong("id_emprunt"));
//                emprunt.setDateEmprunt(result.getDate("date_emprunt"));
//                Ouvrage ouvrage = ouvrageDAO.find(result.getLong("id_ouvrage"));
//                emprunt.setOuvrage(ouvrage);
//                Adherent adherent = adherentDAO.find(result.getLong("id_adherent"));
//                emprunt.setAdherent(adherent);
//                empruntsAdherent.add(emprunt);
//            }
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        return empruntsAdherent;
//    }

//    @Override
//    public List<Emprunt> findAllOutOfTime() {
//        List<Emprunt> empruntsOutOfTime = new ArrayList<>();
//        try {
//            ResultSet result = this.connect
//                    .createStatement(
//                    ).executeQuery(
//                            "SELECT * FROM Emprunt WHERE (current_date - date_emprunt) > 14 "
//                    );
//
//            while (result.next()){
//                Emprunt emprunt = new Emprunt();
//                emprunt.setId(result.getLong("id_emprunt"));
//                emprunt.setDateEmprunt(result.getDate("date_emprunt"));
//                Ouvrage ouvrage = ouvrageDAO.find(result.getLong("id_ouvrage"));
//                emprunt.setOuvrage(ouvrage);
//                Adherent adherent = adherentDAO.find(result.getLong("id_adherent"));
//                emprunt.setAdherent(adherent);
//                empruntsOutOfTime.add(emprunt);
//            }
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        return empruntsOutOfTime;
//    }
}
