package fr.afpa.mediatheque.dao.impl;

import fr.afpa.mediatheque.dao.DAO;
import fr.afpa.mediatheque.dao.ReservationEmpruntDAO;
import fr.afpa.mediatheque.models.Adherent;
import fr.afpa.mediatheque.models.Emprunt;
import fr.afpa.mediatheque.models.Ouvrage;
import fr.afpa.mediatheque.models.Reservation;
import org.hibernate.Query;
import org.hibernate.Transaction;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ReservationDaoIMP extends GenericDAO<Reservation> implements ReservationEmpruntDAO<Reservation> {

    OuvrageDaoIMP ouvrageDAO= new OuvrageDaoIMP();
    AdherentDaoIMP adherentDAO= new AdherentDaoIMP();

    public ReservationDaoIMP() {
        super(Reservation.class);
    }


    @Override
    public Reservation findByIdOuvrage(long idOuvrage) {
        Transaction transaction = session.beginTransaction();
        Query query = session.getNamedQuery("findReservationByIdOuvrage");
        query.setParameter("idOuvrage", idOuvrage);
        Reservation reservation = (Reservation) query.uniqueResult();
        //System.out.println(Reservation.getIdReservation());

        transaction.commit();
        return reservation;
    }

    @Override
    public List<Reservation> finAlldByIdAdherent(long idAdherent) {
        Transaction transaction = super.session.beginTransaction();
        Query query = session.getNamedQuery("findAllReservationByIdAdherent");
        query.setParameter("iduser", idAdherent);
        ArrayList<Reservation> reservations = (ArrayList<Reservation>) query.list();
        transaction.commit();
        return reservations;
    }

    @Override
    public List<Reservation> findAllOutOfTime() {
        Query query=session.createSQLQuery("SELECT * FROM Reservation WHERE (current_date - datereservation) > 14");
        List<Reservation> reservations= (List<Reservation>)   query.list();
        session.getTransaction().commit();
        return reservations;
    }


//    public Reservation findByIdOuvrage(long idOuvrage) {
//
//        Reservation reservation = new Reservation();
//        try {
//            ResultSet result = this.connect
//                    .createStatement(
//                    ).executeQuery(
//                            "SELECT * FROM Reservation WHERE id_ouvrage = " + idOuvrage
//                    );
//
//            while (result.next()){
//                reservation.setId(result.getLong("id_reservation"));
//                reservation.setDateReservation(result.getDate("date_reservation"));
//                Ouvrage ouvrage = ouvrageDAO.find(result.getLong("id_ouvrage"));
//                reservation.setOuvrage(ouvrage);
//                Adherent adherent = adherentDAO.find(result.getLong("id_adherent"));
//                reservation.setAdherent(adherent);
//            }
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        return reservation;
//    }

//    @Override
//    public List<Reservation>  finAlldByIdAdherent(long idAdherent) {
//
//        List<Reservation> reservationsAdherent = new ArrayList<>();
//        try {
//            ResultSet result = this.connect
//                    .createStatement(
//                    ).executeQuery(
//                            "SELECT * FROM Reservation WHERE id_adherent = " + idAdherent
//                    );
//
//            while (result.next()){
//                Reservation reservation = new Reservation();
//                reservation.setId(result.getLong("id_reservation"));
//                reservation.setDateReservation(result.getDate("date_reservation"));
//                Ouvrage ouvrage = ouvrageDAO.find(result.getLong("id_ouvrage"));
//                reservation.setOuvrage(ouvrage);
//                Adherent adherent = adherentDAO.find(result.getLong("id_adherent"));
//                reservation.setAdherent(adherent);
//                reservationsAdherent.add(reservation);
//            }
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        return reservationsAdherent;
//    }

//    @Override
//    public List<Reservation> findAllOutOfTime() {
//        List<Reservation> reservationsOutOfTime = new ArrayList<>();
//        try {
//            ResultSet result = this.connect
//                    .createStatement(
//                    ).executeQuery(
//                            "SELECT * FROM Reservation WHERE (current_date -date_reservation) > 7"
//                    );
//
//            while (result.next()){
//                Reservation reservation = new Reservation();
//                reservation.setId(result.getLong("id_reservation"));
//                reservation.setDateReservation(result.getDate("date_reservation"));
//                Ouvrage ouvrage = ouvrageDAO.find(result.getLong("id_ouvrage"));
//                reservation.setOuvrage(ouvrage);
//                Adherent adherent = adherentDAO.find(result.getLong("id_adherent"));
//                reservation.setAdherent(adherent);
//                reservationsOutOfTime.add(reservation);
//            }
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        return reservationsOutOfTime;
//    }
}
