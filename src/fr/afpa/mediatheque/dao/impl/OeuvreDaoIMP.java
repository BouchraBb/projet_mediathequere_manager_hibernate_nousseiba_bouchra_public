package fr.afpa.mediatheque.dao.impl;

import fr.afpa.mediatheque.dao.DAO;
import fr.afpa.mediatheque.dao.IOeuvreDAO;
import fr.afpa.mediatheque.models.Emprunt;
import fr.afpa.mediatheque.models.Oeuvre;
import org.hibernate.Query;
import org.hibernate.Transaction;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class OeuvreDaoIMP extends GenericDAO<Oeuvre> implements IOeuvreDAO {


    public OeuvreDaoIMP() {
        super(Oeuvre.class);
    }

    public void deleteOuvrages(Oeuvre obj) {
        Transaction transaction = session.beginTransaction();
        Query query = session.getNamedQuery("deleteOuvrages");
        query.setParameter("idOeuvre", obj.getIdOeuvre());
        transaction.commit();
    }

    @Override
    public Oeuvre findByTitle(String title) {
        Transaction transaction = session.beginTransaction();
        Query query = session.getNamedQuery("findByTitle");
        query.setParameter("title", title);
        Oeuvre oeuvre = (Oeuvre) query.uniqueResult();
        //System.out.println(emprunt.getIdEmprunt());

        transaction.commit();

        return oeuvre;
}


    @Override
    public List<Oeuvre> findAndFilter(String[] keywords, List<String> filtres) {

        String keyword = "";

        for(int i =0; i < keywords.length; i++) {
            if(i < keywords.length-1) {
                keyword += "'%"+ keywords[i] + "%',";
            } else {
                keyword += "'%"+ keywords[i] + "%'";
            }
        }

        String selectedGenre = "";

        for(String filtre : filtres) {
            if(filtres.indexOf(Objects.toString(filtre)) < filtres.size()-1) {
                selectedGenre += "'" +filtre+"',";
            } else {
                selectedGenre += "'" +filtre+"'";
            }
        }

        Query query=session.createSQLQuery("SELECT idoeuvre, titre, genre, anneeparution, exemplaires, exemplairesdispo, prix FROM oeuvre WHERE LOWER(titre) LIKE ANY (array["+keyword+"]) and genre in (" + selectedGenre + ")");
        List<Oeuvre> oeuvres= (List<Oeuvre>)   query.list();
        session.getTransaction().commit();
        return oeuvres;
    }




//    public void deleteOuvrages(Oeuvre obj) {
//        try {
//            this.connect
//                    .createStatement(
//
//                    ).executeUpdate(
//                            "DELETE FROM Ouvrage WHERE id_oeuvre = " + obj.getId()
//                    );
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//    }


//    @Override
//    public Oeuvre findByTitle(String title) {
//        Oeuvre oeuvre = new Oeuvre();
//        try {
//            ResultSet result = this.connect
//                    .createStatement(
//                    ).executeQuery(
//                            "SELECT * FROM Oeuvre WHERE titre = '" + title + "'"
//                    );
//
//            while (result.next()){
//
//                oeuvre.setId(result.getLong("id_oeuvre"));
//                oeuvre.setTitre(result.getString("titre"));
//                oeuvre.setGenre(result.getString("genre"));
//                oeuvre.setAnneeParution(result.getDate("annee_parution"));
//                oeuvre.setExemplaires(result.getInt("exemplaires"));
//                oeuvre.setExemplairesDispo(result.getInt("exemplaires_dispo"));
//                oeuvre.setPrix(result.getDouble( "prix"));
//            }
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        return oeuvre;
//    }

//    @Override
//    public List<Oeuvre> findAndFilter(String[] keywords, List<String> filtres) {
//
//        List<Oeuvre> list = new ArrayList<>();


//        String keyword = "";
//
//        for(int i =0; i < keywords.length; i++) {
//            if(i < keywords.length-1) {
//                keyword += "'%"+ keywords[i] + "%',";
//            } else {
//                keyword += "'%"+ keywords[i] + "%'";
//            }
//        }
//
//        String selectedGenre = "";
//
//        for(String filtre : filtres) {
//            if(filtres.indexOf(Objects.toString(filtre)) < filtres.size()-1) {
//                selectedGenre += "'" +filtre+"',";
//            } else {
//                selectedGenre += "'" +filtre+"'";
//            }
//        }
//
//        System.out.println(selectedGenre);
//        // session.createSqlQuery("requette")
//
//        String query = "SELECT id_oeuvre, titre, genre, annee_parution, exemplaires, exemplaires_dispo, prix " +
//                "FROM oeuvre " +
//                "WHERE LOWER(titre) LIKE ANY (array["+keyword+"]) and genre in (" + selectedGenre + ");";
//
//
//        //TODO : requête à vérifier sur psql
//        System.out.println(query);
//
//        try {
//            ResultSet result = this.connect.createStatement().executeQuery(query);
//
//            while (result.next()){
//                Oeuvre oeuvre = new Oeuvre();
//                oeuvre.setId(result.getLong("id_oeuvre"));
//                oeuvre.setTitre(result.getString("titre"));
//                oeuvre.setGenre(result.getString("genre"));
//                oeuvre.setAnneeParution(result.getDate("annee_parution"));
//                oeuvre.setExemplaires(result.getInt("exemplaires"));
//                oeuvre.setExemplairesDispo(result.getInt("exemplaires_dispo"));
//                oeuvre.setPrix(result.getDouble( "prix"));
//                list.add(oeuvre);
//            }
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }

//        return list;
//
//    }


//    public String concatFiltres(List<String> filtres) {
//
//        String selectedGenre = "";
//
//        for(String filtre : filtres) {
//            if(filtres.indexOf(Objects.toString(filtre)) < filtres.size()-1) {
//                selectedGenre += "'" +filtre+"',";
//            } else {
//                selectedGenre += "'" +filtre+"'";
//            }
//        }
//
//        return selectedGenre;
//    }

}
