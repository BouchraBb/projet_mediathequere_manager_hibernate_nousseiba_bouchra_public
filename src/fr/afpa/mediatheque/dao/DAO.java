package fr.afpa.mediatheque.dao;

import java.sql.Connection;
import java.util.List;

public interface DAO<T> {

    public  T findById(long id)  ;

    public  void save(T obj);

    public  void update(T obj);

    public  void delete(T obj);
    public List<T> findAll();


}
