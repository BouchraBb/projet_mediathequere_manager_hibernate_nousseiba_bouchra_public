package fr.afpa.mediatheque.dao;

import fr.afpa.mediatheque.models.Adherent;

public interface IAdherentDAO {

    Adherent findByLogin(String login);

}
