package fr.afpa.mediatheque.services;

import fr.afpa.mediatheque.models.Emprunt;
import fr.afpa.mediatheque.models.Reservation;

import java.util.List;

public interface IReservationService {
    public void create(Reservation reservation);

    public void delete(Reservation reservation);

    public Reservation find(Long id);
    public  Reservation findByIdOuvrage (long idOuvrage)  ;
    public List<Reservation> finAlldByIdAdherent(long idAdherent)  ;

    List<Reservation> ShowAllReservationsOutOfTime();
}
