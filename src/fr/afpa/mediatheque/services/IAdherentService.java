package fr.afpa.mediatheque.services;

import fr.afpa.mediatheque.models.Adherent;

public interface IAdherentService {
    Adherent findByLogin(String login);
    void create(Adherent adherent);
    void update(Adherent adherent);
    void deleteAccount(Adherent adherent);
}
