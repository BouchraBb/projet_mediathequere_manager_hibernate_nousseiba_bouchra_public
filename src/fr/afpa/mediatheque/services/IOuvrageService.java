package fr.afpa.mediatheque.services;

import fr.afpa.mediatheque.models.Oeuvre;
import fr.afpa.mediatheque.models.Ouvrage;

import java.util.List;

public interface IOuvrageService {
    public void createOuvrages(Oeuvre createdOeuvre) ;
    public void create(Oeuvre oeuvre);

    //public void update(Ouvrage ouvrage);

    public void delete(Ouvrage ouvrage);

    public Ouvrage find(Long id);


    void deleteOuvragesRendus();

    List<Long> findOeuvreToUpDate();

    List<Ouvrage> findOuvrageDisponibles(Ouvrage ouvrage);
}
