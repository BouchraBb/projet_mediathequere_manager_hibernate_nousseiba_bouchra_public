package fr.afpa.mediatheque.services;

import fr.afpa.mediatheque.models.Emprunt;

import java.util.List;


public interface IEmpruntService {
    public void create(Emprunt emprunt);

    public void delete(Emprunt emprunt);

    public Emprunt find(Long id);
    public  Emprunt findByIdOuvrage (long idOuvrage)  ;
    public List<Emprunt> finAlldByIdAdherent(long idAdherent)  ;

    List<Emprunt> ShowAllEmpruntsOutOfTime();
}
