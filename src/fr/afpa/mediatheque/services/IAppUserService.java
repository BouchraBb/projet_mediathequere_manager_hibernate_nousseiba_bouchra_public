package fr.afpa.mediatheque.services;

import fr.afpa.mediatheque.models.Adherent;
import fr.afpa.mediatheque.models.AppUser;

public interface IAppUserService {
    AppUser findByLogin(String login);
    void create(AppUser appUser);
    void update(AppUser appUser);
    void deleteAccount(AppUser appUser);
}
