package fr.afpa.mediatheque.services;

import fr.afpa.mediatheque.models.Oeuvre;

import java.util.List;

public interface IOeuvreService {

    public Long create(Oeuvre oeuvre);

    public void update(Oeuvre oeuvre);

    public void delete(Oeuvre oeuvre);

    public Oeuvre find(Long id);

    Oeuvre findByTitle(String title);

    public void updateGrowExemplaires(Oeuvre oeuvre);

    public void updateLessExemplaires(Oeuvre oeuvre);

    List<Oeuvre> findAllByKeywords(String[] keywords, List<String> filtres);

}

