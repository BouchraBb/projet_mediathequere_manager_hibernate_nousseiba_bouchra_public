package fr.afpa.mediatheque.services;

import fr.afpa.mediatheque.models.Personne;

import java.util.List;

public interface IPersonneService {

    public Personne findById(long id );
    public List<Personne> findAll();
    public void create (Personne personne);
}
