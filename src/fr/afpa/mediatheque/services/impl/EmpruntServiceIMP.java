package fr.afpa.mediatheque.services.impl;

import fr.afpa.mediatheque.dao.DAO;
import fr.afpa.mediatheque.dao.ReservationEmpruntDAO;
import fr.afpa.mediatheque.dao.impl.AdherentDaoIMP;
import fr.afpa.mediatheque.dao.impl.EmpruntDaoIMP;
import fr.afpa.mediatheque.dao.impl.OeuvreDaoIMP;
import fr.afpa.mediatheque.dao.impl.OuvrageDaoIMP;
import fr.afpa.mediatheque.models.Adherent;
import fr.afpa.mediatheque.models.Emprunt;
import fr.afpa.mediatheque.models.Oeuvre;
import fr.afpa.mediatheque.models.Ouvrage;
import fr.afpa.mediatheque.services.IEmpruntService;

import java.util.List;


public class EmpruntServiceIMP implements IEmpruntService {
    EmpruntDaoIMP empruntDao = new EmpruntDaoIMP();
    OuvrageDaoIMP ouvrageDAO= new OuvrageDaoIMP();
    OeuvreDaoIMP oeuvreDAO= new OeuvreDaoIMP();
    AdherentDaoIMP adherentDao = new AdherentDaoIMP();



    @Override
    public void create(Emprunt emprunt) {
        empruntDao.save(emprunt);

        Ouvrage ouvrage = ouvrageDAO.findById(emprunt.getOuvrage().getIdOuvrage());

        ouvrage.setAvailable(false);
        ouvrageDAO.update(ouvrage);

        Oeuvre oeuvre= oeuvreDAO.findById(emprunt.getOuvrage().getOeuvre().getIdOeuvre());
        oeuvre.setExemplairesDispo(oeuvre.getExemplairesDispo()-1);
        oeuvreDAO.update(oeuvre);

    }

    @Override
    public void delete(Emprunt emprunt) {
        empruntDao.delete(emprunt);

        Ouvrage ouvrage = ouvrageDAO.findById(emprunt.getOuvrage().getIdOuvrage());

        ouvrage.setAvailable(true);
        ouvrage.setRserved(false);
        ouvrageDAO.update(ouvrage);

        Oeuvre oeuvre= oeuvreDAO.findById(emprunt.getOuvrage().getOeuvre().getIdOeuvre());
        oeuvre.setExemplairesDispo(oeuvre.getExemplairesDispo()+1);
        oeuvreDAO.update(oeuvre);
    }

    @Override
    public Emprunt find(Long id) {
        return empruntDao.findById(id);
    }

    @Override
    public Emprunt findByIdOuvrage(long idOuvrage) {
        return empruntDao.findByIdOuvrage(idOuvrage);
    }

    @Override
    public List<Emprunt> finAlldByIdAdherent(long idAdherent) {
        return empruntDao.finAlldByIdAdherent(idAdherent);
    }

    @Override
    public List<Emprunt> ShowAllEmpruntsOutOfTime() {
        return empruntDao.findAllOutOfTime();
    }
}
