package fr.afpa.mediatheque.services.impl;

import fr.afpa.mediatheque.dao.DAO;
import fr.afpa.mediatheque.dao.ReservationEmpruntDAO;
import fr.afpa.mediatheque.dao.impl.AdherentDaoIMP;
import fr.afpa.mediatheque.dao.impl.OeuvreDaoIMP;
import fr.afpa.mediatheque.dao.impl.OuvrageDaoIMP;
import fr.afpa.mediatheque.dao.impl.ReservationDaoIMP;
import fr.afpa.mediatheque.models.Adherent;
import fr.afpa.mediatheque.models.Oeuvre;
import fr.afpa.mediatheque.models.Ouvrage;
import fr.afpa.mediatheque.models.Reservation;
import fr.afpa.mediatheque.services.IReservationService;

import java.util.List;

public class ReservationServiceIMP implements IReservationService {
    ReservationDaoIMP reservationDao = new ReservationDaoIMP();
    OuvrageDaoIMP ouvrageDAO= new OuvrageDaoIMP();
    AdherentDaoIMP adherentDao = new AdherentDaoIMP();
    OeuvreDaoIMP oeuvreDAO= new OeuvreDaoIMP();

    @Override
    public void create(Reservation reservation) {
        reservationDao.save(reservation);
        Ouvrage ouvrage = ouvrageDAO.findById(reservation.getOuvrage().getIdOuvrage());
        ouvrage.setAvailable(false);
        ouvrage.setRserved(true);
        ouvrageDAO.update(ouvrage);
        Oeuvre oeuvre= oeuvreDAO.findById(reservation.getOuvrage().getOeuvre().getIdOeuvre());
        oeuvre.setExemplairesDispo(oeuvre.getExemplairesDispo()-1);
        oeuvreDAO.update(oeuvre);
    }

    @Override
    public void delete(Reservation reservation) {
        reservationDao.delete(reservation);
        Ouvrage ouvrage = ouvrageDAO.findById(reservation.getOuvrage().getIdOuvrage());
        ouvrage.setAvailable(true);
        ouvrage.setRserved(false);
        ouvrageDAO.update(ouvrage);
        Oeuvre oeuvre= oeuvreDAO.findById(reservation.getOuvrage().getOeuvre().getIdOeuvre());
        oeuvre.setExemplairesDispo(oeuvre.getExemplairesDispo()+1);
        oeuvreDAO.update(oeuvre);
    }

    @Override
    public Reservation find(Long id) {
        return reservationDao.findById(id);
    }

    @Override
    public Reservation findByIdOuvrage(long idOuvrage) {
        return reservationDao.findByIdOuvrage(idOuvrage);
    }

    @Override
    public List<Reservation> finAlldByIdAdherent(long idAdherent) {
        return reservationDao.finAlldByIdAdherent(idAdherent);
    }

    @Override
    public List<Reservation> ShowAllReservationsOutOfTime() {
        return reservationDao.findAllOutOfTime();
    }

}
