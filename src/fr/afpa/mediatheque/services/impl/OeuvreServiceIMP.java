package fr.afpa.mediatheque.services.impl;

import fr.afpa.mediatheque.dao.impl.OeuvreDaoIMP;
import fr.afpa.mediatheque.models.Oeuvre;
import fr.afpa.mediatheque.services.IOeuvreService;

import java.util.List;

public class OeuvreServiceIMP implements IOeuvreService {

    OeuvreDaoIMP oeuvreDao = new OeuvreDaoIMP();

    @Override
    public Long create(Oeuvre oeuvre) {
        return oeuvreDao.save(oeuvre);

    }

    @Override
    public void update(Oeuvre oeuvre) {
        oeuvreDao.update(oeuvre);
    }

    @Override
    public void delete(Oeuvre oeuvre) {
        oeuvreDao.delete(oeuvre);
    }

    @Override
    public Oeuvre find(Long id) {
        return oeuvreDao.findById(id);
    }

    @Override
    public Oeuvre findByTitle(String title) {
        return oeuvreDao.findByTitle(title);
    }

   @Override
    public List<Oeuvre> findAllByKeywords(String[] keywords, List<String> filtres) {
        return oeuvreDao.findAndFilter(keywords, filtres);
    }

    public void updateGrowExemplaires(Oeuvre oeuvre) {
        oeuvre.setExemplaires(oeuvre.getExemplaires() +1);
        oeuvre.setExemplairesDispo(oeuvre.getExemplairesDispo() +1);
    }
    public void updateLessExemplaires(Oeuvre oeuvre) {
        oeuvre.setExemplaires(oeuvre.getExemplaires() -1);
        oeuvre.setExemplairesDispo(oeuvre.getExemplairesDispo() - 1);
    }
}
