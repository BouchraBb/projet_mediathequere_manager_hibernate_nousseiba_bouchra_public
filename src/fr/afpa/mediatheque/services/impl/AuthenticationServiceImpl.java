package fr.afpa.mediatheque.services.impl;


import fr.afpa.mediatheque.dao.AuthenticationDao;
import fr.afpa.mediatheque.dao.impl.AuthenticationDaoImpl;
import fr.afpa.mediatheque.models.Adherent;
import fr.afpa.mediatheque.models.AppUser;
import fr.afpa.mediatheque.services.AuthenticationService;

public class AuthenticationServiceImpl implements AuthenticationService {

    AuthenticationDao authenticationDao = new AuthenticationDaoImpl();
    @Override
    public AppUser authenticate(String username, String password) throws Exception {
        return authenticationDao.authenticate(username, password);
    }

    @Override
    public Adherent authenticateAdherent(String email, String userPassword) {
        return authenticationDao.authenticateAdherent(email, userPassword);
    }
}
