package fr.afpa.mediatheque.services.impl;

import fr.afpa.mediatheque.dao.DAO;
import fr.afpa.mediatheque.dao.IOuvrageDAO;
import fr.afpa.mediatheque.dao.impl.OuvrageDaoIMP;
import fr.afpa.mediatheque.models.Oeuvre;
import fr.afpa.mediatheque.models.Ouvrage;
import fr.afpa.mediatheque.services.IOuvrageService;

import java.util.List;

public class OuvrageServiceIMP implements IOuvrageService {
    OuvrageDaoIMP ouvrageDAO= new OuvrageDaoIMP();


    public void createOuvrages(Oeuvre createdOeuvre) {
        Ouvrage ouvrage = new Ouvrage(createdOeuvre);
        for (int i=0; i<createdOeuvre.getExemplaires(); i++){
            ouvrageDAO.save(ouvrage);
        }
    }

    @Override
    public void create(Oeuvre oeuvre) {
        Ouvrage ouvrage = new Ouvrage(oeuvre);
        ouvrageDAO.save(ouvrage);
    }

    @Override
    public void delete(Ouvrage ouvrage) {
        ouvrageDAO.delete(ouvrage);
    }

    @Override
    public Ouvrage find(Long id) {
        return ouvrageDAO.findById(id);
    }

    @Override
    public void deleteOuvragesRendus() {
        ouvrageDAO.deleteOuvragesRendus();
    }

    @Override
    public List<Long> findOeuvreToUpDate() {
        return ouvrageDAO.findOuvrages();
    }

    @Override
    public List<Ouvrage> findOuvrageDisponibles(Ouvrage ouvrage) {
       return  ouvrageDAO.findOuvragesOeuvreDisponible(ouvrage.getOeuvre().getIdOeuvre());
    }

}
