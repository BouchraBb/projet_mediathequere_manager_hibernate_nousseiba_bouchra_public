package fr.afpa.mediatheque.services.impl;

import fr.afpa.mediatheque.dao.DAO;
import fr.afpa.mediatheque.dao.impl.PersonneDaoIMP;
import fr.afpa.mediatheque.models.Personne;
import fr.afpa.mediatheque.services.IPersonneService;

import java.util.List;

public class PersonneServiceIMP implements IPersonneService {

    PersonneDaoIMP personneDao = new PersonneDaoIMP();
    public Personne findById(long id ){
        return personneDao.findById(id);
    }

    public List<Personne> findAll(){
        return personneDao.findAll();
    }

    public void create (Personne personne){
         personneDao.save(personne);
    }


}
