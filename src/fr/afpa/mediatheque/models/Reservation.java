package fr.afpa.mediatheque.models;

import fr.afpa.mediatheque.utils.Constantes;
import org.hibernate.engine.spi.Mapping;

import javax.persistence.*;
import java.sql.Date;
import java.util.Calendar;

@Entity
@NamedQueries({@NamedQuery(name="findReservationByIdOuvrage" ,query="SELECT e FROM Reservation e WHERE e.ouvrage.idOuvrage = :idOuvrage"),
@NamedQuery(name="findAllReservationByIdAdherent" ,query="SELECT e FROM Reservation e WHERE e.adherent.idUser = :idUser")})
public class Reservation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idReservation;
    private Date dateReservation;

    @OneToOne
    @JoinColumn(name="idOuvrage")
    private Ouvrage ouvrage ;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "idUser")
    private Adherent adherent;

    public Reservation( Date dateReservation, Ouvrage ouvrage, Adherent adherent) {
        this.dateReservation = dateReservation;
        this.ouvrage = ouvrage;
        this.adherent = adherent;
    }
    public Reservation() {

    }

    public Long getIdReservation() {
        return idReservation;
    }

    public void setIdReservation(Long idReservation) {
        this.idReservation = idReservation;
    }

    public Date getDateReservation() {
        return dateReservation;
    }

    public void setDateReservation(Date dateReservation) {
        this.dateReservation = dateReservation;
    }

    public Ouvrage getOuvrage() {
        return ouvrage;
    }

    public void setOuvrage(Ouvrage ouvrage) {
        this.ouvrage = ouvrage;
    }

    public Adherent getAdherent() {
        return adherent;
    }

    public void setAdherent(Adherent adherent) {
        this.adherent = adherent;
    }



    @Override
    public String toString() {
        return "Reservation{" +
                "id=" + idReservation +
                ", dateReservation=" + Constantes.formatter.format(dateReservation.toLocalDate()) +"\n"+
                ", ouvrage=" + ouvrage +"\n"+
                ", adherent=" + adherent +"\n"+
                '}';
    }
}
