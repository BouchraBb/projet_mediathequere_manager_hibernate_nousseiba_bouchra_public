package fr.afpa.mediatheque.models;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Personne {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idPersonne;
    private String nom;
    private String prenom;
    private String profession;

    @ManyToMany
    List<Oeuvre> oeuvres=new ArrayList<>();


    public Personne() {

    }

    public Personne(String nom, String prenom, String profession) {
        this.nom = nom;
        this.prenom = prenom;
        this.profession = profession;
    }


    public Long getIdPersonne() {
        return idPersonne;
    }

    public void setIdPersonne(Long idPersonne) {
        this.idPersonne = idPersonne;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }


    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }



    @Override
    public String toString() {
        return "Personne{" +
                "id=" + idPersonne +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", profession='" + profession + '\'' +
                '}';
    }

    public List<Oeuvre> getOeuvres() {
        return oeuvres;
    }

    public void setOeuvres(List<Oeuvre> oeuvres) {
        this.oeuvres = oeuvres;
    }

    public void addOeuvre(Oeuvre oeuvre) {
        this.getOeuvres().add(oeuvre);
        oeuvre.addPersonne(this);
    }

    public void removeOeuvre(Oeuvre oeuvre) {
        this.getOeuvres().remove(oeuvre);
        oeuvre.removePersonne(this);
    }
}
