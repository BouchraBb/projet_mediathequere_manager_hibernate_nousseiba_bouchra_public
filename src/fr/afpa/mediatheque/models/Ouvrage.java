package fr.afpa.mediatheque.models;

import fr.afpa.mediatheque.utils.Constantes;

import javax.persistence.*;
import java.sql.Date;

@Entity
@NamedQueries({@NamedQuery(name= "deleteOuvragesRendus", query="DELETE FROM Ouvrage o WHERE  o.isAvailable IS true and isToBeDeleted IS true" ),
@NamedQuery(name= "findOuvrages", query="SELECT o.oeuvre.idOeuvre FROM Ouvrage o WHERE o.isAvailable IS true and o.isToBeDeleted IS true" ),
@NamedQuery(name="findOuvragesOeuvreDisponible",query="SELECT o FROM Ouvrage o WHERE o.oeuvre.idOeuvre= :id and o.isAvailable IS true and isRserved IS false"),
@NamedQuery(name="deleteOuvrages", query="DELETE FROM Ouvrage o WHERE o.oeuvre.idOeuvre = :idOeuvre")})
public class Ouvrage {

    /* @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idAchat;
    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name="idProduit")
    private Produit produit;
*/

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idOuvrage;
    private Boolean isAvailable;
    private Boolean isRserved;
    private Boolean isToBeDeleted;

    @OneToOne(mappedBy="ouvrage")
     private Reservation reservation;

    @OneToOne(mappedBy="ouvrage")
    private Emprunt emprunt;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name="idOeuvre")
    private Oeuvre oeuvre;

    public Ouvrage() {

    }


    public Ouvrage(Oeuvre oeuvre) {
        this.isAvailable = true;
        this.isRserved = false;
        this.isToBeDeleted= false;
        this.oeuvre = oeuvre;
    }


    public Long getIdOuvrage() {
        return idOuvrage;
    }

    public void setIdOuvrage(Long idOuvrage) {
        this.idOuvrage = idOuvrage;
    }

    public Boolean getAvailable() {
        return isAvailable;
    }

    public void setAvailable(Boolean available) {
        isAvailable = available;
    }

    public Boolean getRserved() {
        return isRserved;
    }

    public void setRserved(Boolean rserved) {
        isRserved = rserved;
    }

    public Boolean getToBeDeleted() {
        return isToBeDeleted;
    }

    public void setToBeDeleted(Boolean toBeDeleted) {
        isToBeDeleted = toBeDeleted;
    }

    public boolean isAvailable() {
        return isAvailable;
    }

    public void setAvailable(boolean available) {
        isAvailable = available;
    }

    public boolean isRserved() {
        return isRserved;
    }

    public void setRserved(boolean rserved) {
        isRserved = rserved;
    }

    public boolean isToBeDeleted() {
        return isToBeDeleted;
    }

    public void setToBeDeleted(boolean toBeDeleted) {
        isToBeDeleted = toBeDeleted;
    }

    public Oeuvre getOeuvre() {
        return oeuvre;
    }

    public void setOeuvre(Oeuvre oeuvre) {
        this.oeuvre = oeuvre;
    }

    public Reservation getReservation() {
        return reservation;
    }

    public void setReservation(Reservation reservation) {
        this.reservation = reservation;
    }

    public Emprunt getEmprunt() {
        return emprunt;
    }

    public void setEmprunt(Emprunt emprunt) {
        this.emprunt = emprunt;
    }



    @Override
    public String toString() {
        return "Ouvrage{" +
                "id=" + idOuvrage +
                ", isAvailable=" + isAvailable +
                ", isRserved=" + isRserved +
                ", oeuvre=" + oeuvre +
                '}';
    }


}
