package fr.afpa.mediatheque.models;

import javax.persistence.*;

@Entity
@Inheritance(strategy= InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="TYPE_USER")
@DiscriminatorValue(value="User")
@NamedQueries({@NamedQuery(name = "findByLogin", query = "SELECT a FROM AppUser a WHERE a.email = :email"),
@NamedQuery(name ="authenticate",query= "SELECT a FROM AppUser a WHERE a.email = :email AND a.userPassword = :password ")})
public class AppUser {

    @Id
    @GeneratedValue(strategy =GenerationType.IDENTITY)
    protected long idUser;
    protected String nom;
    protected String prenom;
    protected String email;
    protected String userPassword;
    protected String userRole;

    public AppUser() {
    }

    public AppUser(String nom, String prenom, String email, String userPassword, String userRole) {
        this.nom = nom;
        this.prenom = prenom;
        this.email = email;
        this.userPassword = userPassword;
        this.userRole = userRole;
    }

    public AppUser(String nom, String prenom, String email, String userPassword) {
        this.nom = nom;
        this.prenom = prenom;
        this.email = email;
        this.userPassword = userPassword;
    }


    public long getIdUser() {
        return idUser;
    }

    public void setIdUser(long idUser) {
        this.idUser = idUser;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getUserRole() {
        return userRole;
    }

    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }



    @Override
    public String toString() {
        return "AppUser{" +
                "id=" + idUser +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", email='" + email + '\'' +
                ", userPassword='" + userPassword + '\'' +
                ", userRole='" + userRole + '\'' +
                '}';
    }
}
