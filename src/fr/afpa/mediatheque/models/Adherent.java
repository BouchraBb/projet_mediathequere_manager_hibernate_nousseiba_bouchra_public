package fr.afpa.mediatheque.models;

import fr.afpa.mediatheque.utils.Constantes;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@DiscriminatorValue(value="ADHERENT")
@NamedQueries({
@NamedQuery(name = "findAdherentByLogin", query = "SELECT a FROM Adherent a WHERE a.email = :email"),
@NamedQuery(name ="authenticateAdherent",query= "SELECT a FROM Adherent a WHERE a.email = :email AND a.userPassword = :password ")})
public class Adherent extends AppUser{

    private Double solde;

    @OneToMany(mappedBy = "adherent")
    private List<Emprunt> emprunts=new ArrayList<>();

    @OneToMany(mappedBy = "adherent")
    private List<Reservation> reservations=new ArrayList<>();
    public Adherent() {
        this.solde = 00.00;
    }

    public Adherent(String nom, String prenom, String email, String userPassword) {
        super(nom, prenom, email, userPassword);
        super.setUserRole(Constantes.ROLE_ADHERENT);
        this.solde = 00.00;
    }

    public Adherent(String nom, String prenom, String email, String userPassword, String adherent, Double solde) {
        super(nom, prenom, email, userPassword);
        userRole= Constantes.ROLE_ADHERENT;
        this.solde = solde;
    }

    public Double getSolde() {
        return solde;
    }

    public void setSolde(Double solde) {
        this.solde = solde;
    }

    public List<Emprunt> getEmprunts() {
        return emprunts;
    }

    public void setEmprunts(List<Emprunt> emprunts) {
        this.emprunts = emprunts;
    }

    public List<Reservation> getReservations() {
        return reservations;
    }

    public void setReservations(List<Reservation> reservations) {
        this.reservations = reservations;
    }

    @Override
    public String toString() {
        return "AppUser{" +
                "id=" + idUser +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", email='" + email + '\'' +
                ", userPassword='" + userPassword + '\'' +
                ", userRole='" + userRole + '\'' +
                ", solde='" + solde + '\'' +

                '}';
    }
}
