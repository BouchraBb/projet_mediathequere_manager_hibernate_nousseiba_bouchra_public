package fr.afpa.mediatheque.models;


import fr.afpa.mediatheque.utils.Constantes;

import javax.persistence.*;
import java.sql.Date;
import java.time.LocalDate;
import java.time.temporal.TemporalAccessor;
import java.util.ArrayList;
import java.util.List;

@Entity
@NamedQueries({
@NamedQuery(name= "findByTitle", query="SELECT o FROM Oeuvre o WHERE o.titre = :titre")})
public  class Oeuvre {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idOeuvre;
    private String titre;
    private String genre;

    private Date anneeParution;
    private Integer exemplaires;

    private Integer exemplairesDispo;

    private Double prix;


    @OneToMany(mappedBy= "oeuvre")
    private List<Ouvrage> ouvrages=new ArrayList<>();

    @ManyToMany(cascade={CascadeType.PERSIST} , mappedBy = "oeuvres")
    private List<Personne> personnes=new ArrayList<>();
    public Oeuvre() {
    }

    public Oeuvre(String genre, Double prix) {
        this.genre = genre;
        this.prix = prix;
    }


    public Oeuvre(String titre, String genre, Date anneeParution, Integer exemplaires, Double prix) {
        this.titre = titre;
        this.genre = genre;
        this.anneeParution = anneeParution;
        this.exemplaires = exemplaires;
        this.exemplairesDispo = exemplaires;
        this.prix = prix;
    }


    public Long getIdOeuvre() {
        return idOeuvre;
    }

    public void setIdOeuvre(Long idOeuvre) {
        this.idOeuvre = idOeuvre;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }


    public Double getPrix() {
        return prix;
    }

    public void setPrix(Double prix) {
        this.prix = prix;
    }

    public Date getAnneeParution() {
        return anneeParution;
    }

    public void setAnneeParution(Date anneeParution) {
        this.anneeParution = anneeParution;
    }

    public Integer getExemplaires() {
        return exemplaires;
    }

    public void setExemplaires(Integer exemplaires) {
        this.exemplaires = exemplaires;
    }

    public Integer getExemplairesDispo() {
        return exemplairesDispo;
    }

    public void setExemplairesDispo(Integer exemplairesDispo) {
        this.exemplairesDispo = exemplairesDispo;
    }



    public List<Ouvrage> getOuvrages() {
        return ouvrages;
    }

    public void setOuvrages(List<Ouvrage> ouvrages) {
        this.ouvrages = ouvrages;
    }

    public List<Personne> getPersonnes() {
        return personnes;
    }

    public void setPersonnes(List<Personne> personnes) {
        this.personnes = personnes;
    }

    public void addPersonne(Personne personne){
        this.getPersonnes().add(personne);
        personne.addOeuvre(this);
    }
    public void removePersonne(Personne personne){
        this.getPersonnes().remove(personne);
        personne.removeOeuvre(this);
    }


    @Override
    public String toString() {
        return "Oeuvre{" +
                "id=" + idOeuvre +
                ", titre='" + titre + '\'' +
                ", genre='" + genre + '\'' +
                ", anneeParution=" + Constantes.formatter.format( anneeParution.toLocalDate()) +
                ", exemplaires=" + exemplaires +
                ", exemplairesDispo=" + exemplairesDispo +
                ", prix=" + prix +
                '}';
    }
}
