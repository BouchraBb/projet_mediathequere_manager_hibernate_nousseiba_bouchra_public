package fr.afpa.mediatheque.models;

import javax.persistence.*;
import java.sql.Date;

@Entity
@NamedQueries({@NamedQuery(name="findByIdOuvrage" ,query="SELECT e FROM Emprunt e WHERE e.ouvrage.idOuvrage = :idOuvrage"),
@NamedQuery(name="finAlldByIdAdherent" ,query="SELECT e FROM Emprunt e WHERE e.adherent.idUser = :idUser")})
public class Emprunt {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idEmprunt;
    private Date dateEmprunt;

    @OneToOne
    @JoinColumn(name="idOuvrage")
    private Ouvrage ouvrage ;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "idUser")
    private Adherent adherent;



    public Emprunt(Date dateEmprunt, Ouvrage ouvrage, Adherent adherent) {
        this.dateEmprunt = dateEmprunt;
        this.ouvrage = ouvrage;
        this.adherent = adherent;
    }

    public Emprunt() {

    }

    public Long getIdEmprunt() {
        return idEmprunt;
    }

    public void setIdEmprunt(Long idEmprunt) {
        this.idEmprunt = idEmprunt;
    }

    public Date getDateEmprunt() {
        return dateEmprunt;
    }

    public void setDateEmprunt(Date dateEmprunt) {
        this.dateEmprunt = dateEmprunt;
    }

    public Ouvrage getOuvrage() {
        return ouvrage;
    }

    public void setOuvrage(Ouvrage ouvrage) {
        this.ouvrage = ouvrage;
    }

    public Adherent getAdherent() {
        return adherent;
    }

    public void setAdherent(Adherent adherent) {
        this.adherent = adherent;
    }




    @Override
    public String toString() {
        return "Emprunt{" +
                "id=" + idEmprunt +
                ", dateEmprunt=" + dateEmprunt +
                ", ouvrage=" + ouvrage +
                ", adherent=" + adherent +
                '}';
    }
}
