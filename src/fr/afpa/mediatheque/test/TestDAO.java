package fr.afpa.mediatheque.test;

import fr.afpa.mediatheque.dao.impl.GenericDAO;
import fr.afpa.mediatheque.models.Oeuvre;
import fr.afpa.mediatheque.services.IOeuvreService;
import fr.afpa.mediatheque.services.impl.OeuvreServiceIMP;
import fr.afpa.mediatheque.utils.HibernateUtilTest;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class TestDAO {

    IOeuvreService oeuvreService = new OeuvreServiceIMP();

    @BeforeEach
    public void instanceSession(){
         GenericDAO.session = HibernateUtilTest.getSessionFactoryTest().openSession();

    }
    @Test
    public void finById(){
        //given
         int id = 1 ;
         // when
       // Oeuvre oueuvere= oeuvreService.find(id);


    }

    @Test
    public void create (){

        // given
         Oeuvre oeuvre = new Oeuvre("FILM",2.3);
         //when
        Long id = oeuvreService.create(oeuvre);

        System.out.println(id);
        Oeuvre oeuvreFind = (Oeuvre) GenericDAO.session.get(Oeuvre.class, id);
        System.out.println(oeuvreFind.getIdOeuvre());

        // verifier
        Assertions.assertNotNull(oeuvreFind);
        Assertions.assertEquals(oeuvre.getIdOeuvre(), oeuvreFind.getIdOeuvre());


    }


//    @Test
//    public void create (){
//
//        // given
//        Oeuvre oeuvre =  GenericDAO.session.get(Ouve, id);
//        //when
//        ....
//        Integer id =oeuvreService.update(oeuvre);
//
//        GenericDAO.session.get(Oeuve, id);
//
//
//    }


}
