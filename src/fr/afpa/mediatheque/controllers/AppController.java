package fr.afpa.mediatheque.controllers;

import fr.afpa.mediatheque.models.Adherent;
import fr.afpa.mediatheque.models.Oeuvre;
import fr.afpa.mediatheque.views.AppView;

public class AppController {

    AdherentController adherentController = new AdherentController();
    AuthenticationController authenticationController = new AuthenticationController();
    OeuvreController oeuvreController = new OeuvreController();

    AppView appView = new AppView();

    public String displayMenu() {
        String choice = appView.mainMenu();
        appView.switchChoice(choice, this);
        return choice;
    }

    public void createAccount() {
        adherentController.createAccount();
    }

    public void signIn() {
        //authenticationController.authenticate();
        Adherent adherent = adherentController.signIn();
        adherentController.displayMenu();
    }

    public void searchAllOeuvres() {
        oeuvreController.searchAll();
    }
}
