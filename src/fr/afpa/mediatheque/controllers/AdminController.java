package fr.afpa.mediatheque.controllers;

import fr.afpa.mediatheque.models.AppUser;
import fr.afpa.mediatheque.services.impl.AppUserService;
import fr.afpa.mediatheque.views.AdminView;

public class AdminController {

    AdminView adminView = new AdminView();
    AppUserService appUserService = new AppUserService();


    public String displayMenu() {
        String choice = "";
        do {
            choice = adminView.mainMenu();
            adminView.switchChoice(choice, this);

        } while(!choice.equalsIgnoreCase("0"));

        return choice;
    }

    public void createAppUser() {
        AppUser newUser = adminView.promptAppUser();
        appUserService.create(newUser);
    }
}
