package fr.afpa.mediatheque.controllers;

import fr.afpa.mediatheque.models.Emprunt;
import fr.afpa.mediatheque.models.Personne;
import fr.afpa.mediatheque.services.IPersonneService;
import fr.afpa.mediatheque.services.impl.PersonneServiceIMP;
import fr.afpa.mediatheque.views.AppView;
import fr.afpa.mediatheque.views.PersonView;

import java.util.ArrayList;
import java.util.List;

public class PersonneController {

    PersonView personView = new PersonView();
    IPersonneService personneService = new PersonneServiceIMP();
    AppView appView= new AppView();


    public List<Personne> addPersonsToOeuvre( String role){

        int nombre = appView.promptNumber(" nombre de "+ role + " participants  ");
        List<Personne> personnes = personneService.findAll() ;
        personView.showAllPersonne(personnes);

        String choice="";
        long id ;
        List<Personne> pers = new ArrayList<>();
        for (int i = 0; i < nombre; i++) {
            choice =  personView.menuPersonne(nombre, role);
        }
        switch (choice) {
            case "1": {
                id = appView.promptID("du "+ role + "choisi : ");
                personnes.add(personneService.findById(id));

            }
            case "2": {
                Personne personne = personView.promptPersonne(role);
                personneService.create(personne);
                personnes.add(personne);
            }
        }

        return personnes;
    }


}
