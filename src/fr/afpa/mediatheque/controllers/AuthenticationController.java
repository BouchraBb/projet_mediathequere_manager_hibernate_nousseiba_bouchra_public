package fr.afpa.mediatheque.controllers;



import fr.afpa.mediatheque.models.Adherent;
import fr.afpa.mediatheque.models.AppUser;
import fr.afpa.mediatheque.services.AuthenticationService;
import fr.afpa.mediatheque.services.impl.AuthenticationServiceImpl;

import java.util.Scanner;

public class AuthenticationController {

    private final Scanner scanner = new Scanner(System.in);
    private AuthenticationService authenticationService = new AuthenticationServiceImpl();


  public AppUser authenticate(String login, String mdp) throws Exception {
        return authenticationService.authenticate(login, mdp);
    }

    public Adherent authenticateAdherent(String email, String userPassword) {
      return authenticationService.authenticateAdherent(email, userPassword);
    }
}
