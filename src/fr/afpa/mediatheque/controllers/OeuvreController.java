package fr.afpa.mediatheque.controllers;

import fr.afpa.mediatheque.models.Oeuvre;
import fr.afpa.mediatheque.services.impl.OeuvreServiceIMP;
import fr.afpa.mediatheque.views.OeuvreView;

import java.util.List;

public class OeuvreController {

    OeuvreView oeuvreView = new OeuvreView();
    OeuvreServiceIMP oeuvreServiceIMP = new OeuvreServiceIMP();

    public void searchAll() {
        List<String> filtres = oeuvreView.promptFilter();
        String [] keywords = oeuvreView.promptKeywords();
        List<Oeuvre> listOeuvres = oeuvreServiceIMP.findAllByKeywords(keywords, filtres);
        oeuvreView.display(listOeuvres);
    }
}
