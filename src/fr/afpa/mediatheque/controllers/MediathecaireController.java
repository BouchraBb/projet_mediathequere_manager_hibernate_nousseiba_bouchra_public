package fr.afpa.mediatheque.controllers;

import fr.afpa.mediatheque.models.*;
import fr.afpa.mediatheque.services.IAdherentService;
import fr.afpa.mediatheque.services.IEmpruntService;
import fr.afpa.mediatheque.services.IReservationService;
import fr.afpa.mediatheque.services.impl.*;
import fr.afpa.mediatheque.views.AppView;
import fr.afpa.mediatheque.views.MediathecaireView;
import fr.afpa.mediatheque.views.OeuvreView;


import java.sql.Date;
import java.util.List;

public class MediathecaireController {

    MediathecaireView mediathecaireView = new MediathecaireView();
    OeuvreView oeuvreView = new OeuvreView();
    AppUserService appUserService = new AppUserService();
    IAdherentService adherentService = new AdherentServiceIMP();
    OeuvreServiceIMP oeuvreService = new OeuvreServiceIMP();
    OuvrageServiceIMP ouvrageService = new OuvrageServiceIMP();
    AdherentController adherentController = new AdherentController();
    IEmpruntService empruntService = new EmpruntServiceIMP();
    IReservationService reservationService= new ReservationServiceIMP();
    AppView appView= new AppView();



    public void createOeuvre() {
        Oeuvre oeuvre = oeuvreView.promptOeuvre();
        oeuvreService.create(oeuvre);
        Oeuvre createdOeuvre = oeuvreService.findByTitle(oeuvre.getTitre());
        ouvrageService.createOuvrages(createdOeuvre);
    }

    public void updateOeuvre() {
        Long id = appView.promptID(" de l'oeuvre");
        Oeuvre oeuvre = oeuvreService.find(id);
        Oeuvre oeuvreUpdated= oeuvreView.modifyOeuvre(oeuvre);
        oeuvreService.update(oeuvreUpdated);
    }

    public void deleteOeuvre() {
        Long id = appView.promptID(" de l'oeuvre");
        Oeuvre oeuvre = oeuvreService.find(id);
        oeuvreService.delete(oeuvre);
    }

    public String displayMenu() {
        String choice = "";
        do {
            choice = mediathecaireView.mainMenu();
            mediathecaireView.switchChoice(choice, this);
        } while(!choice.equalsIgnoreCase("0"));
        return choice;
    }

    public void createOuvrage() {
        Long id = appView.promptID(" de l'oeuvre");
        Oeuvre oeuvre = oeuvreService.find(id);
        oeuvreService.updateGrowExemplaires(oeuvre);
        oeuvreService.update(oeuvre);
        ouvrageService.create(oeuvre);
    }

    public void deleteOuvrage() {
        Long id = appView.promptID(" de l'ouvrage ");
        Ouvrage ouvrage =ouvrageService.find(id);
            if (ouvrage.isRserved() == false) {
                ouvrageService.delete(ouvrage);
                Long idOeuvre = ouvrage.getOeuvre().getIdOeuvre();
                Oeuvre oeuvre = oeuvreService.find(idOeuvre);
                oeuvreService.updateLessExemplaires(oeuvre);
                oeuvreService.update(oeuvre);

            } else {
                ouvrage.setToBeDeleted(true);
            }
    }

    public void deleteOuvragesRendus() {
        List<Long> IdOeuvres= ouvrageService.findOeuvreToUpDate();
        Oeuvre oeuvre ;
        for (int i=0 ; i<IdOeuvres.size(); i++){
            oeuvre = oeuvreService.find(IdOeuvres.get(i));
            oeuvreService.updateLessExemplaires(oeuvre);
            oeuvreService.update(oeuvre);
        }
        ouvrageService.deleteOuvragesRendus();
    }


    public void createAdherent() {
        adherentController.createAccount();
    }

    public void showAllAdherants() {
        List<Adherent> adherents = appUserService.getAll();
        mediathecaireView.displayAllAdherents(adherents);
    }

    public void deleteAdherant() {
        adherentController.deleteAccount();
    }

    public void saveEmprunt(){
        String login =  mediathecaireView.promptAdherantEmail();
        Adherent adherent=  adherentService.findByLogin( login);

        Long id = appView.promptID(" de l'ouvrage");
        Date dateEmprunt = mediathecaireView.promptDateRE();
        Ouvrage ouvrage =ouvrageService.find(id);

        if(!ouvrage.isRserved() && ouvrage.isAvailable()) {
            Emprunt emprunt = new Emprunt(dateEmprunt, ouvrage, adherent);
            empruntService.create(emprunt);

        }else{
            List<Ouvrage> ouvragesdispo= ouvrageService.findOuvrageDisponibles(ouvrage);
            mediathecaireView.NotAvailabaleOuvrage(ouvrage);
            if(ouvragesdispo.size()>0) {
                mediathecaireView.displayAllOuvrages(ouvragesdispo);
                id = appView.promptID(" de l'ouvrage");
                ouvrage = ouvrageService.find(id);
                Emprunt emprunt = new Emprunt(dateEmprunt, ouvrage, adherent);
                empruntService.create(emprunt);
            }
        }

    }

    public void saveReservation(){
        String login =  mediathecaireView.promptAdherantEmail();
        Adherent adherent=  adherentService.findByLogin( login);
        Date dateReservation = mediathecaireView.promptDateRE();
        Long id = appView.promptID(" de l'ouvrage ");
        Ouvrage ouvrage =ouvrageService.find(id);

        if(!ouvrage.isRserved() && ouvrage.isAvailable()) {
            Reservation reservation = new Reservation(dateReservation,ouvrage, adherent);
            reservationService.create(reservation);

        }else{
            List<Ouvrage> ouvragesdispo= ouvrageService.findOuvrageDisponibles(ouvrage);
            mediathecaireView.NotAvailabaleOuvrage(ouvrage);
            if(ouvragesdispo.size()>0) {
                mediathecaireView.displayAllOuvrages(ouvragesdispo);
                id = appView.promptID(" de l'ouvrage");
                ouvrage = ouvrageService.find(id);
                Reservation reservation = new Reservation(dateReservation,ouvrage, adherent);
                reservationService.create(reservation);
            }
        }

    }


    public void deleteReservation() {
        String choice =  mediathecaireView.promptChoiceSearch();
        long id = mediathecaireView.promptID();
        Reservation reservation= null;
        switch (choice) {
            case "1": {
                reservation= reservationService.find(id);
            }
            case "2": {
                reservation= reservationService.findByIdOuvrage(id);
            }
        }
        if(reservation!=null) {
            reservationService.delete(reservation);
        }

    }

    public void deleteEmprunt() {
        String choice =  mediathecaireView.promptChoiceSearch();
        long id = mediathecaireView.promptID();
        Emprunt emprunt= null;
        switch (choice) {
            case "1": {
                emprunt= empruntService.find(id);
            }
            case "2": {
                emprunt= empruntService.findByIdOuvrage(id);
            }
        }
        if(emprunt!=null) {
            empruntService.delete(emprunt);
        }
    }

    public void ShowAllAdherentReservations() {
        String login =  mediathecaireView.promptAdherantEmail();
        Adherent adherent=  adherentService.findByLogin( login);
        List<Reservation> reservations = reservationService.finAlldByIdAdherent(adherent.getIdUser());
        mediathecaireView.displayAllReservation(reservations);

    }

    public void ShowAllAdherentEmprunts() {
        String login =  mediathecaireView.promptAdherantEmail();
        Adherent adherent=  adherentService.findByLogin( login);
        List<Emprunt> emprunts = empruntService.finAlldByIdAdherent(adherent.getIdUser());
        mediathecaireView.displayAllEmprunt(emprunts);

    }

    public void ShowAllEmpruntsOutOfTime() {
        List<Emprunt> empruntsOutOfTime = empruntService.ShowAllEmpruntsOutOfTime();
        mediathecaireView.displayAllEmprunt(empruntsOutOfTime);
        if(empruntsOutOfTime.size()<1){
            mediathecaireView.notFind();
        }
    }
    public void ShowAllReservationsOutOfTime() {
        List<Reservation> reservationsOutOfTime = reservationService.ShowAllReservationsOutOfTime();
        mediathecaireView.displayAllReservation(reservationsOutOfTime);
        if(reservationsOutOfTime.size()<1){
            mediathecaireView.notFind();
        }
    }

    public void deleteEmpruntsOutOfTime() {
        List<Reservation> reservationsOutOfTime = reservationService.ShowAllReservationsOutOfTime();
        Reservation reservationToDelte;
        for (Reservation reservation :reservationsOutOfTime) {
            reservationToDelte = reservationService.find(reservation.getIdReservation());
            reservationService.delete(reservationToDelte);
        }

    }
}
