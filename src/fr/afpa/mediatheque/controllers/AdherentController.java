package fr.afpa.mediatheque.controllers;

import fr.afpa.mediatheque.models.Adherent;
import fr.afpa.mediatheque.services.IAdherentService;
import fr.afpa.mediatheque.services.impl.AdherentServiceIMP;
import fr.afpa.mediatheque.views.AdherentView;
import fr.afpa.mediatheque.views.AppView;

public class AdherentController {

    Adherent adherentSession;
    AdherentView adherentView = new AdherentView();
    IAdherentService adherentServiceIMP = new AdherentServiceIMP();

    AuthenticationController authenticationController = new AuthenticationController();
    OeuvreController oeuvreController = new OeuvreController();

    public String displayMenu() {
        String choice = "";
        do {
            choice = adherentView.mainMenu();
            adherentView.switchChoice(choice, this);
        } while(!choice.equalsIgnoreCase("0"));
        return choice;
    }

    public void createAccount() {
        adherentServiceIMP.create(adherentView.promptAdherant());
    }

    public void updateAccount() {
        String login = adherentView.promptLogin();
        Adherent adherent = adherentServiceIMP.findByLogin(login);
        adherentServiceIMP.update(adherentView.promptUpdate(adherent));
    }

    public void showAccount() {
        Adherent adherent = adherentView.signIn();
        //TODO : remplacer 'findByLogin' par une methode d'authentification login/mdp
        Adherent adherentFind = adherentServiceIMP.findByLogin(adherent.getEmail());
        adherentView.display(adherentFind);

    }

    public void deleteAccount() {
        Adherent adherent = adherentView.signIn();
        Adherent adherentFind = adherentServiceIMP.findByLogin(adherent.getEmail());
        adherentServiceIMP.deleteAccount(adherentFind);

    }

    public Adherent signIn() {
        Adherent adherent = adherentView.signIn();
        try {
            adherent = authenticationController.authenticateAdherent(adherent.getEmail(), adherent.getUserPassword());
            System.out.println(adherent.toString());
            adherentSession= adherent;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        System.out.println("Connecté en tant que " + adherent.toString());
        return adherent;
    }

    public void searchOeuvre() {
        oeuvreController.searchAll();
    }

    public void reloadSolde() {
        AppView.showMessage("Votre solde actuel : "+adherentSession.getSolde() +"");
        double somme = adherentView.promptdouble("le montant à ajouter : ");
        adherentSession.setSolde(adherentSession.getSolde() + somme);
        System.out.println(adherentSession.getSolde());
        System.out.println(adherentSession.toString());
        adherentServiceIMP.update(adherentSession);


    }
}
