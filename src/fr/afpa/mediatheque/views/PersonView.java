package fr.afpa.mediatheque.views;

import fr.afpa.mediatheque.models.Personne;

import java.util.List;

public class PersonView {

    public Personne promptPersonne(String role){

        System.out.println("Nom : ");
        String nom = AppView.scanner.nextLine();

        System.out.println("Prenom : ");
        String prenom = AppView.scanner.nextLine();

        return new Personne( nom,  prenom, role);
    }

    public String menuPersonne(int nombre, String role){
         String choice = "";
        do{
            System.out.println("[1]- Ajouter  un " + role + " existant ");
            System.out.println("[2]- Ajouter  un nouveau " + role );


        }while(!"1".equals(choice) && !"2".equals(choice) );
        return choice;

    }
    public void showAllPersonne(List<Personne> personnes){
        for(Personne personne : personnes) {
            System.out.println("--------------------");
            System.out.println(personne.toString());
            System.out.println();
        }
    }

}
