package fr.afpa.mediatheque.views;

import fr.afpa.mediatheque.controllers.PersonneController;
import fr.afpa.mediatheque.models.Oeuvre;
import fr.afpa.mediatheque.models.Personne;
import fr.afpa.mediatheque.utils.Constantes;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

public class OeuvreView {
    PersonneController personneController = new PersonneController();

    public Oeuvre promptOeuvre() {

        Oeuvre newOeuvre = switchTypeOeuvre();

        System.out.println("Saisir le titre de l'oeuvre : ");
        String titre = AppView.scanner.nextLine();

        System.out.println("Saisir la date de parution yyyy-mm-jj: ");
        String date = AppView.scanner.nextLine();

        Date date1 = Date.valueOf(date);

        System.out.println("Nombre d'exemplaires ? ");
        Integer exemplaires = Integer.parseInt(AppView.scanner.nextLine());

        newOeuvre.setTitre(titre);
        newOeuvre.setExemplaires(exemplaires);
        newOeuvre.setExemplairesDispo(exemplaires);
        newOeuvre.setAnneeParution(date1);

        return newOeuvre;
    }

    public Oeuvre switchTypeOeuvre() {

        Oeuvre newOeuvre = new Oeuvre();
        String choice = "";
        boolean isNotGood= false;
       List<Personne> personneList;

        do {
            System.out.println("Choisir le type d'oeuvre : ");
            System.out.println("\t [1] - Livre");
            System.out.println("\t [2] - Disque");
            System.out.println("\t [3] - Film");
            choice = AppView.scanner.nextLine();

            switch (choice) {
                case "1": {
                    newOeuvre.setGenre(Constantes.GENRE_LIVRE);
                    newOeuvre.setPrix(Constantes.PRIX_LIVRE);

                    personneList= personneController.addPersonsToOeuvre(Constantes.PRO_AUTEUR);
                    for (Personne personne: personneList
                         ) {
                        newOeuvre.addPersonne(personne);
                    }
                    return newOeuvre;
                }

                case "2": {
                    newOeuvre.setGenre(Constantes.GENRE_DISQUE);
                    newOeuvre.setPrix(Constantes.PRIX_DISQUE);

                    personneList= personneController.addPersonsToOeuvre(Constantes.PRO_INTERPRETE);
                    for (Personne personne: personneList
                    ) {
                        newOeuvre.addPersonne(personne);
                    }
                    return newOeuvre;
                }

                case "3": {
                    newOeuvre.setGenre(Constantes.GENRE_FILM);
                    newOeuvre.setPrix(Constantes.PRIX_FILM);

                    personneList= personneController.addPersonsToOeuvre(Constantes.PRO_ACTEUR);
                    for (Personne personne: personneList
                    ) {
                        newOeuvre.addPersonne(personne);
                    }
                    return newOeuvre;
                }

                default: {
                    System.out.println("Choix invalide.");
                    isNotGood = true;
                    break;
                }
            }

        } while(isNotGood);

        return newOeuvre;
    }



    public Oeuvre modifyOeuvre(Oeuvre updatedOeuvre) {
        String choice = "";
        boolean isNotGood= false;

        do {
            System.out.println("Choisir la colonne à modifier : ");
            System.out.println("\t [1] - Titre");
            System.out.println("\t [2] - prix ");
            choice = AppView.scanner.nextLine();

            switch (choice) {
                case "1": {
                    System.out.println("Saisir le nouveau titre");
                    updatedOeuvre.setGenre(AppView.scanner.nextLine());
                    return updatedOeuvre;
                }

                case "2": {
                    System.out.println("Saisir le nouveau prix");
                    updatedOeuvre.setPrix(Double.parseDouble(AppView.scanner.nextLine()));
                    return updatedOeuvre;
                }


                default: {
                    System.out.println("Choix invalide.");
                    isNotGood = true;
                    break;
                }
            }

        } while(isNotGood);

        return updatedOeuvre;
    }

    public String [] promptKeywords() {
        System.out.println("Entrer un titre, ou des mots clés.. : ");
        return AppView.scanner.nextLine().split(" ");
    }

    public void display(List <Oeuvre> listOeuvres) {
        for (Oeuvre oeuvre : listOeuvres) {
            System.out.println();
            System.out.println(oeuvre.toString());
            System.out.println();
        }
    }

    public List<String> promptFilter() {
        List<String>choices = new ArrayList<>();
        System.out.println("Sélectionner des genres : ");
        System.out.println("[1] - LIVRE");
        System.out.println("[2] - DISQUE");
        System.out.println("[3] - FILM");

        String [] choice = AppView.scanner.nextLine().split(" ");

        for(int i = 0; i < choice.length; i++) {
            if(choice[i].equalsIgnoreCase("1")) {
                choices.add(Constantes.GENRE_LIVRE);
            }
            if(choice[i].equalsIgnoreCase("2")) {
                choices.add(Constantes.GENRE_DISQUE);
            }
            if(choice[i].equalsIgnoreCase("3")) {
                choices.add(Constantes.GENRE_FILM);
            }
        }

        System.out.println("Filtre selectionnées : " + choices.toString());
        return choices;
    }
}
