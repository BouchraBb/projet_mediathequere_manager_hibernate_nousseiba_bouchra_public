package fr.afpa.mediatheque.utils;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

public class HibernateUtilTest {

     static SessionFactory sessionFactoryTest;

    public static SessionFactory getSessionFactoryTest() {


        if(sessionFactoryTest==null) {
            Configuration configuration = new Configuration().configure("fr/afpa/mediatheque/config/hibernateTest.cfg.xml");

            ServiceRegistry serviceRegistry = new ServiceRegistryBuilder().applySettings(
                    configuration.getProperties()).buildServiceRegistry();

            sessionFactoryTest = configuration.buildSessionFactory(serviceRegistry);
        }

        return sessionFactoryTest;
    }
}
