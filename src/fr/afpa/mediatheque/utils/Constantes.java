package fr.afpa.mediatheque.utils;

import java.time.format.DateTimeFormatter;

public class Constantes {

    public static final String  PRO_ACTEUR = "ACTEUR";
    public static final String  PRO_AUTEUR = "AUTEUR";
    public static final String  PRO_INTERPRETE = "INTERPRETE";



    public static final String  ROLE_ADMIN = "ADMIN";
    public static final String  ROLE_MEDIATHECAIRE = "MEDIATHECAIRE";
    public static final String  ROLE_ADHERENT = "ADHERENT";



    public static final String GENRE_LIVRE=   "LIVRE";
    public static final String GENRE_FILM=  "FILM" ;
    public static final String GENRE_DISQUE=  "DISQUE" ;



    public static final double PRIX_LIVRE= 1.10 ;
    public static final double PRIX_FILM= 3 ;
    public static final double PRIX_DISQUE= 2.50;

    public static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    public static DateTimeFormatter formatterUp = DateTimeFormatter.ofPattern("dd-MM-yyyy");

}
