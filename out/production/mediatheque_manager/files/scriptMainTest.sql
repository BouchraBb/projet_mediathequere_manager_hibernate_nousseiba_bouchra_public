-- init personne
INSERT INTO public.personne (nom, prenom, profession) VALUES('MARS', 'LYLA', 'AUTEUR');
INSERT INTO public.personne (nom, prenom, profession) VALUES('MCAVOY', 'JAMES', 'ACTEUR');
INSERT INTO public.personne (nom, prenom, profession) VALUES('FOY', 'CLAIRE', 'ACTEUR');
commit;
-- init oueuvre
INSERT INTO public.oeuvre (anneeparution,exemplaires, exemplairesdispo,genre,prix,titre )
VALUES( '08-02-2023',3,3,'LIVRE',1.10,'I am not your Soulmate' );

INSERT INTO public.oeuvre (anneeparution,exemplaires, exemplairesdispo,genre,prix,titre )
VALUES( '23-06-2021',2,1,'LIVRE',1.10,'Un visage pour deux' );

INSERT INTO public.oeuvre (anneeparution,exemplaires, exemplairesdispo,genre,prix,titre )
VALUES( '20-09-2020',2,1,'FILM',3,'My son' );

INSERT INTO public.oeuvre (anneeparution,exemplaires, exemplairesdispo,genre,prix,titre )
VALUES('13-05-2009',2,2,'FILM',3,'Millenium' );
commit;

-- init ouvrage


-- init personne-oeuve
INSERT INTO public.personne_oeuvre(personnes_idpersonne ,oeuvres_idoeuvre) VALUES(1,1);
INSERT INTO public.personne_oeuvre (personnes_idpersonne ,oeuvres_idoeuvre)VALUES(1,2);
INSERT INTO public.personne_oeuvre(personnes_idpersonne ,oeuvres_idoeuvre) VALUES(2,3);
INSERT INTO public.personne_oeuvre (personnes_idpersonne ,oeuvres_idoeuvre) VALUES(3,3);
INSERT INTO public.personne_oeuvre (personnes_idpersonne ,oeuvres_idoeuvre) VALUES(3,4);


-- init users
INSERT INTO public.appuser (type_user, email,nom, prenom,  userpassword, userrole, solde) VALUES( 'ADMIN', 'jerome@gmail.com', 'DUMONT', 'JEROME', 'root','ADMIN',NULL);
INSERT INTO public.appuser (type_user, email,nom, prenom,  userpassword, userrole, solde) VALUES('MEDIATHECAIRE', 'julesd@gmail.com',  'DUPIRE', 'JULES','root','MEDIATHECAIRE', NULL);
INSERT INTO public.appuser (type_user, email,nom, prenom,  userpassword, userrole, solde) VALUES('MEDIATHECAIRE',  'valeriem@gmail.com', 'MAIETTE', 'VALERIE','root' ,'MEDIATHECAIRE', NULL);
INSERT INTO public.appuser (type_user, email,nom, prenom,  userpassword, userrole, solde) VALUES('MEDIATHECAIRE','staphanedd@gmail.com', 'DUPIRE', 'STEPHANE',  'root','MEDIATHECAIRE', NULL);

-- init data Adherent
INSERT INTO public.appuser (type_user, email,nom, prenom,  userpassword, userrole, solde)VALUES('ADHERENT','nadeged@gmail.com' ,'DUPONT', 'NADEGE',  'root', 'ADHERENT', 100);
INSERT INTO public.appuser(type_user, email,nom, prenom,  userpassword, userrole, solde) VALUES('ADHERENT', 'jules@gmail.com','DUPIRE', 'JULES','root', 'ADHERENT', 50);
INSERT INTO public.appuser(type_user, email,nom, prenom,  userpassword, userrole, solde) VALUES('ADHERENT','mariesd@gmail.com', 'DUMONt', 'MARIE', 'root', 'ADHERENT', 70);

-- init data Ouvrage
INSERT INTO public.ouvrage
(isavailable,isrserved, istobedeleted, idoeuvre)
VALUES(true, false, false, 1);
INSERT INTO public.ouvrage
(isavailable,isrserved, istobedeleted, idoeuvre)
VALUES(true, false, false, 1);
INSERT INTO public.ouvrage
(isavailable,isrserved, istobedeleted, idoeuvre)
VALUES(true, false, false, 1);

INSERT INTO public.ouvrage
(isavailable,isrserved, istobedeleted, idoeuvre)
VALUES(false, true, false, 2);
INSERT INTO public.ouvrage
(isavailable,isrserved, istobedeleted, idoeuvre)
VALUES(true, false, false, 2);

INSERT INTO public.ouvrage
(isavailable,isrserved, istobedeleted, idoeuvre)
VALUES(true, false, false, 3);
INSERT INTO public.ouvrage
(isavailable,isrserved, istobedeleted, idoeuvre)
VALUES(false, false, false, 3);

INSERT INTO public.ouvrage
(isavailable,isrserved, istobedeleted, idoeuvre)
VALUES(true, false, false, 4);
INSERT INTO public.ouvrage
(isavailable,isrserved, istobedeleted, idoeuvre)
VALUES(true, false, false, 4);
commit;


select * FROM appuser
select * from personne
 select * from oeuvre
select * from  personne_oeuvre