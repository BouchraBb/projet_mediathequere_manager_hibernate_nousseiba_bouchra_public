drop table if exists Emprunt;
commit;
drop table if exists Reservation ;
commit;
drop table if exists Ouvrage ;
commit;
drop table if exists App_User;
commit;
drop table if exists Adherent;
commit;
drop table if exists figurer;
commit;
drop table if exists Personne ;
commit;
drop table if exists Oeuvre;
commit;


CREATE TABLE Oeuvre(
   id_oeuvre serial ,
   titre VARCHAR(50),
   genre VARCHAR(50),
   annee_parution DATE,
   exemplaires INT,
   exemplaires_dispo INT,
   prix numeric(10,2),
   PRIMARY KEY(id_oeuvre)
);
commit;
-- init data oueuvre
INSERT INTO public.oeuvre (titre, genre, annee_parution, exemplaires, exemplaires_dispo, prix)
VALUES('I am not your Soulmate', 'LIVRE', '08-02-2023', 3, 3, 1.10);

INSERT INTO public.oeuvre (titre, genre, annee_parution, exemplaires, exemplaires_dispo, prix)
values('Un visage pour deux', 'LIVRE', '23-06-2021', 2, 1, 1.10);

INSERT INTO public.oeuvre (titre, genre, annee_parution, exemplaires, exemplaires_dispo, prix)
values('My son', 'FILM', '20-09-2020', 2, 1, 3);

INSERT INTO public.oeuvre (titre, genre, annee_parution, exemplaires, exemplaires_dispo, prix)
values('Millenium', 'FILM', '13-05-2009',2, 2, 3);
commit;


CREATE TABLE Personne(
   id_personne serial ,
   nom VARCHAR(50),
   prenom VARCHAR(50),
   profession VARCHAR(50) NOT NULL,
   PRIMARY KEY(id_Personne)
);
commit;
-- init data Personne
INSERT INTO public.personne (nom, prenom, profession) VALUES('MARS', 'LYLA', 'AUTEUR');
INSERT INTO public.personne (nom, prenom, profession) VALUES('MCAVOY', 'JAMES', 'ACTEUR');
INSERT INTO public.personne (nom, prenom, profession) VALUES('FOY', 'CLAIRE', 'ACTEUR');
commit;


CREATE TABLE figurer(
   id_oeuvre INT,
   id_Personne INT,
   PRIMARY KEY(id_oeuvre, id_Personne),
   FOREIGN KEY(id_oeuvre) REFERENCES Oeuvre(id_oeuvre),
   FOREIGN KEY(id_Personne) REFERENCES Personne(id_Personne)
);
commit;
-- init data figurer
INSERT INTO public.figurer (id_oeuvre,id_Personne) VALUES(1,1);
INSERT INTO public.figurer (id_oeuvre,id_Personne) VALUES(2,1);
INSERT INTO public.figurer (id_oeuvre,id_Personne) VALUES(3,2);
INSERT INTO public.figurer (id_oeuvre,id_Personne) VALUES(3,3);
INSERT INTO public.figurer (id_oeuvre,id_Personne) VALUES(4,3);


CREATE TABLE App_User(
   id_app_user serial,
   nom VARCHAR(50),
   prenom VARCHAR(50),
   email VARCHAR(50),
   user_password VARCHAR(50),
   user_role VARCHAR(50) NOT NULL,
   PRIMARY KEY(id_app_user),
   UNIQUE(email)
);
commit;
-- init data App_User
INSERT INTO public.app_user (nom, prenom, email, user_password, user_role) VALUES('DUMONT', 'JEROME', 'jerome@gmail.com', 'root', 'ADMIN)');
INSERT INTO public.app_user (nom, prenom, email, user_password, user_role) VALUES('DUPIRE', 'JULES', 'julesd@gmail.com', 'root', 'MEDIATHECAIRE');
INSERT INTO public.app_user (nom, prenom, email, user_password, user_role) VALUES('MAIETTE', 'VALERIE', 'valeriem@gmail.com', 'root', 'MEDIATHECAIRE');
INSERT INTO public.app_user (nom, prenom, email, user_password, user_role) VALUES('DUPIRE', 'STEPHANE', 'staphanedd@gmail.com', 'root', 'MEDIATHECAIRE');
commit;



CREATE TABLE Adherent(
   id_adherent serial,
   nom VARCHAR(50),
   prenom VARCHAR(50),
   email VARCHAR(50),
   user_password VARCHAR(50),
   user_role VARCHAR(50) NOT NULL,
   solde numeric (10,2),
   PRIMARY KEY(id_adherent),
   UNIQUE(email)
);
commit;
-- init data Adherent
INSERT INTO public.adherent (nom, prenom, email, user_password, user_role, solde) VALUES('DUPONT', 'NADEGE', 'nadeged@gmail.com', 'root', 'ADHERENT', 100);
INSERT INTO public.adherent (nom, prenom, email, user_password, user_role, solde) VALUES('DUPIRE', 'JULES', 'jules@gmail.com', 'root', 'ADHERENT', 50);
INSERT INTO public.adherent (nom, prenom, email, user_password, user_role, solde) VALUES('DUMONt', 'MARIE', 'mariesd@gmail.com', 'root', 'ADHERENT', 70);
commit;


CREATE TABLE Ouvrage(
   id_ouvrage  serial ,
   disponible boolean,
   reserve boolean,
   a_supprimer boolean,
   id_oeuvre INT NOT NULL,
   PRIMARY KEY(id_ouvrage),
   FOREIGN KEY(id_oeuvre) REFERENCES Oeuvre(id_oeuvre)
);
commit;
-- init data Ouvrage
INSERT INTO public.ouvrage
(isavailable,isrserved, istobedeleted, id_oeuvre)
VALUES(true, false, false, 1);
INSERT INTO public.ouvrage
(isavailable,isrserved, istobedeleted, id_oeuvre)
VALUES(true, false, false, 1);
INSERT INTO public.ouvrage
(isavailable,isrserved, istobedeleted, id_oeuvre)
VALUES(true, false, false, 1);

INSERT INTO public.ouvrage
(isavailable,isrserved, istobedeleted, id_oeuvre)
VALUES(false, true, false, 2);
INSERT INTO public.ouvrage
(isavailable,isrserved, istobedeleted, id_oeuvre)
VALUES(true, false, false, 2);

INSERT INTO public.ouvrage
(isavailable,isrserved, istobedeleted, id_oeuvre)
VALUES(true, false, false, 3);
INSERT INTO public.ouvrage
(isavailable,isrserved, istobedeleted, id_oeuvre)
VALUES(false, false, false, 3);

INSERT INTO public.ouvrage
(isavailable,isrserved, istobedeleted, id_oeuvre)
VALUES(true, false, false, 4);
INSERT INTO public.ouvrage
(isavailable,isrserved, istobedeleted, id_oeuvre)
VALUES(true, false, false, 4);
commit;

CREATE TABLE Reservation(
   id_reservation serial ,
   date_reservation DATE NOT NULL,
   id_ouvrage INT NOT NULL,
   id_adherent INT NOT NULL,
   UNIQUE(id_ouvrage),
   PRIMARY KEY(id_reservation),
   FOREIGN KEY(id_ouvrage) REFERENCES Ouvrage(id_ouvrage),
   FOREIGN KEY(id_adherent) REFERENCES Adherent(id_adherent)
);
commit;
-- init data Reservation
INSERT INTO public.reservation
(date_reservation, id_ouvrage, id_adherent)
VALUES('23-02-2023', 4, 1);


CREATE TABLE Emprunt(
   id_emprunt serial,
   date_emprunt DATE NOT NULL,
   id_ouvrage INT NOT NULL,
   id_adherent INT NOT NULL,
   PRIMARY KEY(id_emprunt),
   UNIQUE(id_ouvrage),
   FOREIGN KEY(id_ouvrage) REFERENCES Ouvrage(id_ouvrage),
   FOREIGN KEY(id_adherent) REFERENCES Adherent(id_adherent)
);

commit;
-- init data Emprunt
INSERT INTO public.emprunt
(date_emprunt, id_ouvrage, id_adherent)
VALUES('25-02-2023', 7, 2);


commit;



